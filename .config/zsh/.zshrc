#----------------------------------------------#
# File:    .zshrc                              #
# Author:  Adam Jimerson <vendion@gmail.com>   #
# License: WTFPL (http://www.wtfpl.net/about/) #
#----------------------------------------------#

#----------------------
# .zshenv fixes
#----------------------
if [[ ! -f "${HOME}/.zshenv" ]]; then
  echo "# Add environment variables here\n\n# Path alterations\ntypeset -U path\n" > "${HOME}/.zshenv"
  echo 'path=("${HOME}/bin" "${path[@]}")' >> "${HOME}/.zshenv"
  source "${HOME}/.zshenv"
fi

# Always source ~/.zshenv as sometimes it is not automatically.
source "${HOME}/.zshenv"

#----------------------
# Variables
#----------------------
ZDOTDIR=${ZDOTDIR:-${HOME}/.config/zsh}
ZSHDDIR="${HOME}/.config/zsh.d"
ZSH_CACHE_DIR="${HOME}/.cache/zsh"
DIRSTACKFILE="${ZSH_CACHE_DIR}/dirs"
DIRSTACKSIZE=20
downloads="${HOME}/Downloads"
EDITOR="nvim"
PAGER="less"

#----------------------
# History settings
#----------------------
HISTFILE="${XDG_STATE_HOME}/zsh/hist"
HISTSIZE=1000
SAVEHIST=1000

#----------------------
# Keybindings
#----------------------
bindkey -v

#----------------------
# Global Aliases
#----------------------
alias _="sudo "
alias vim="nocorrect vim"
alias zshedit="vim ${HOME}/.zshrc"
alias fucking="sudo "
alias goddammit="sudo "
alias NULL='2> /dev/null'

#----------------------
# Shell Functions
#----------------------
# -- colored manuals
man() {
  env \
    LESS_TERMCAP_mb=$(printf "\e[1;31m") \
    LESS_TERMCAP_md=$(printf "\e[1;31m") \
    LESS_TERMCAP_me=$(printf "\e[0m") \
    LESS_TERMCAP_so=$(printf "\e[0m") \
    LESS_TERMCAP_so=$(printf "\e[1;44;33m") \
    LESS_TERMCAP_ue=$(printf "\e[0m") \
    LESS_TERMCAP_us=$(printf "\e[1;32m") \
    man "$@"
}

run_under_tmux() {
  # Run $1 under session or attach if such session already exist.
  # $2 is optional path, if no specified, will use $1 from $PATH.
  # If you need to pass extra variables, use $2 for it as in
  # example below..
  # Example usage:
  #   torrent() { run_under_tmux 'rtorrent' '/usr/local/rtorrent-git/bin/rtorrent';  }
  #   irc() { run_under_tmux 'irssi' "TERM='screen' command irssi";  }

  # There is a bug in linux's libevent...
  # export EVENT_NOEPOLL=1

  command -v tmux >/dev/null 2>&1 || return 1

  if [ -z "$1"  ]; then return 1; fi
  local name="$1"
  if [ -n "$2"  ]; then
    local execute="$2"
  else
    local execute="command ${name}"
  fi

  if tmux has-session -t "${name}" 2>/dev/null; then
    tmux attach -d -t "${name}"
  else
    tmux new-session -s "${name}" "${execute}" \; set-option status \; set set-titles-string "${name} (tmux@${HOST})"
  fi
}
t() { run_under_tmux rtorrent 'nice -n 19 ionice -c 3 rtorrent';  }
irc() { run_under_tmux chat "TERM='screen' command irssi";  }

reload() {
  exec "${SHELL}" "$@"
}

escape() {
  # Uber useful when you need to translate weird as fuck path into
  # single-argument string.
  local escape_string_input
  echo -n "String to escape: "
  read escape_string_input
  printf '%q\n' "${escape_string_input}"
}

confirm() {
  local answer
  echo -ne "zsh: sure you want to run '%{YELLOW}$*${NC}' [yN]? "
  read -q answer
  if [[ "${answer}" =~ ^[Yy]$ ]]; then
    command "${@}"
  else
    return 1
  fi
}

confirm_wrapper() {
  if [[ "${1}" == '--root' ]]; then
    local as_root='true'
    shift
  fi
  local prefix=''

  if [[ "${as_root}" == 'true' ]] && [[ "${USER}" != 'root' ]]; then
    prefix='sudo'
  fi
  confirm "${prefix} $@"
}

detox() {
  if [[ "$#" -ge 1 ]]; then
    confirm detox "$@"
  else
    command detox "$@"
  fi
}

dot_progress() {
  # Fancy progress function form Landley's Aboriginal Linux,
  # Useful for long rm, tar and such.
  # Usage:
  #   rm -rfv /foo | dot_progress
  local i='0'
  local line=''

  while read line; do
    i="$((i+1))"
    if [[ "${i}" = '25' ]]; then
      printf '.'
      i='0'
    fi
  done
  printf '\n'
}

#----------------------
# Comp settings
#----------------------
fpath+="${ZSHDDIR}/completions"
zmodload zsh/complist
autoload -Uz compinit
compinit -d "$XDG_CACHE_HOME"/zsh/zcompdump-"$ZSH_VERSION"
zstyle :compinstall filename "${HOME}/.zshrc"

zstyle ':completion:*' auto-description 'specify: %d'
zstyle ':completion:*' completer _expand _complete _ignored _correct _approximate
zstyle ':completion:*' expand prefix suffix
zstyle ':completion:*' file-sort name
zstyle ':completion:*' format 'Completing %d items'
zstyle ':completion:*' group-name ''
zstyle ':completion:*' ignore-parents pwd ..
zstyle ':completion:*' list-colors ''
zstyle ':completion:*' list-prompt %SAt %p: Hit TAB for more, or the character to insert%s
zstyle ':completion:*' list-suffixes true
zstyle ':completion:*' matcher-list '' 'm:{[:lower:][:upper:]}={[:upper:][:lower:]}' 'r:|[._-]=** r:|=**' 'l:|=* r:|=*'
zstyle ':completion:*' max-errors 3 numeric
zstyle ':completion:*' menu select=2
zstyle ':completion:*' preserve-prefix '//[^/]##/'
zstyle ':completion:*' prompt '%e errors found'
zstyle ':completion:*' select-prompt %SScrolling active: current selection at %p%s
zstyle ':completion:*' special-dirs true
zstyle ':completion:*' use-compctl true
zstyle ':completion:*' rehash true
zstyle ':completion::complete:*' use-cache 1
zstyle ':completion:*' cache-path "$XDG_CACHE_HOME"/zsh/zcompcache
zstyle ':completion:*:descriptions' format '%U%F{cyan}%d%f%u'

zstyle ':completion:*:pacman:*' force-list always
zstyle ':completion:*:*:pacman:*' menu yes select

zstyle ':completion:*:*:kill:*' menu yes select
zstyle ':completion:*:kill:*' force-list always

zstyle ':completion:*:*:killall:*' menu yes select
zstyle ':completion:*:killall:*' force-list always

#----------------------
# ZSH Features
#----------------------
setopt appendhistory autocd extendedglob nomatch notify promptsubst completealiases

# zmv - a command for renaming files by means of shell patterns.
autoload -U zmv

# zargs, as an alternative to find -exec and xargs.
autoload -U zargs

# Control-x-e to open current line in $EDITOR, awesome when writing functions
# or editng multiline commands.
autoload -U edit-command-line
zle -N edit-command-line
bindkey '^x^e' edit-command-line

# Ignore lines prefixed with '#'
setopt interactivecomments

# Ignore duplicate in history.
setopt hist_ignore_dups

# Prevent record in history if preceding them with at least one space
setopt hist_ignore_space

#----------------------
# Window title
#----------------------
case "${TERM}" in
  termite|*xterm*|rxvt|rxvt-unicode|rxvt-256color|rxvt-unicode-256color|(dt|k|E)term)
    precmd() {
      vcs_info
      print -Pn "\e]0;[%n@%M][%~]%#\a"
    }
    preexec () { print -Pn "\e]0;[%n@%M][%~]%# ($1)\a" }
    ;;
  screen|screen-256color)
    precmd () {
      vcs_info
      print -Pn "\e]83;title \"$1\"\a"
      print -Pn "\e]0;${TERM} - (%L) [%n@%M]%# [%~]\a"
    }
    preexec () {
      print -Pn "\e]83;title \"$1\"\a"
      print -Pn "\e]0;${TERM} - (%L) [%n@%M]%# [%~] ($1)\a"
    }
    ;;
esac

#----------------------
# Prompt
#----------------------
autoload -U colors zsh/terminfo
colors

autoload -Uz vcs_info
zstyle ':vcs_info:*' enable git hg
zstyle ':vcs_info:*' check-for-changes true
zstyle ':vcs_info:git*' formats "%{${fg[cyan]}%}[%{${fg[green]}%}%s%{${fg[cyan]}%}][%{${fg[blue]}%}%r/%S%%{${fg[cyan]}%}][%{${fg[blue]}%}%b%{${fg[yellow]}%}%m%u%c%{${fg[cyan]}%}]%{$reset_color%}"

setprompt() {
  setopt prompt_subst

  # make some aliases for the colors
  for color in 'RED' 'GREEN' 'YELLOW' 'BLUE' 'MAGENTA' 'CYAN' 'WHITE'; do
    eval "PR_${color}"='%{$fg[${(L)color}]%}'
  done
  PR_NO_COLOR="%{$terminfo[sgr0]%}"

  # check the UID
  if [[ "${UID}"  -ge 1000 ]]; then # normal user
    eval "PR_USER"='${PR_GREEN}%n${PR_NO_COLOR}'
    eval "PR_USER_OP"='${PR_GREEN}%#${PR_NO_COLOR}'
  elif [[ "${UID}" -ge 0 ]]; then # root
    eval "PR_USER"='${PR_RED}%n${PR_NO_COLOR}'
    eval "PR_USER_OP"='${PR_RED}%#${PR_NO_COLOR}'
  fi
  # chek if we are on SSH or not
  if [[ -n "${SSH_CLIENT}" || -n "${SSH2_CLIENT}" ]]; then
    eval "PR_HOST"='${PR_YELLOW}%M${PR_NO_COLOR}'
  else
    eval "PR_HOST"='${PR_GREEN}%M${PR_NO_COLOR}'
  fi

  # set the prompt
  PS1=$'${PR_CYAN}[${PR_USER}${PR_CYAN}@${PR_HOST}${PR_CYAN}][${PR_BLUE}%~${PR_CYAN}]${PR_USER_OP} '
  PS2=$'%_>'
  RPROMPT=$'${vcs_info_msg_0_}'
}
setprompt

#----------------------
# ZSH Help
#----------------------
autoload -U run-help
autoload run-help-git
autoload run-help-svn
autoload run-help-svk
alias help=run-help

#----------------------
# Dirstack
# https://goo.gl/1Rbxfn
#----------------------
if [[ ! -d "${ZSH_CACHE_DIR}" ]]; then
  mkdir -p "${ZSH_CACHE_DIR}"
fi

if [[ -f "${DIRSTACKFILE}" ]] && [[ $#dirstack -eq 0 ]]; then
  dirstack=( ${(f)"$(< $DIRSTACKFILE)"} )
  [[ -d $dirstack[1] ]] && cd $dirstack[1]
fi
chpwd() {
  print -l ${PWD} ${(u)dirstack} >${DIRSTACKFILE}
}
setopt autopushd pushdsilent pushdtohome

dbbackup() {
  if [[ ! -e "./backups" ]]; then
    "$(mkdir backups)"
  fi

  if [[ -n "${1}" && -n "${2}" ]]; then
    date="$(date +%Y%m%d)"
    mkdir "backups/${date}"

    if [[ -n "${3}" ]]; then
      fname="${3}"
    else
      fname="${2}_${date}"
    fi

    "$(mysqldump -u ${1} -p${1} --databases ${2}|xz -e > backups/${date}/${fname}.sql.xz)"
  else
    echo "Usage: dbbackup [db info] [database] [filename]\n"
  fi
}

## Remove duplicate entries
setopt pushdignoredups

## This reverts the +/- operators.
setopt pushdminus

# Include user-specified configs
if [[ ! -d "${ZSHDDIR}" ]]; then
  mkdir -p "${ZSHDDIR}"
  if [[ ! -d "${ZSHDDIR}/plugins" ]]; then
    mkdir -p "${ZSHDDIR}/plugins"
    echo "# Put your user-specified conifg here." > "${ZSHDDIR}/plugins/example.zsh"
  fi
fi

if [[ -f "${ZSHDDIR}/plugins.zsh" ]]; then
  source "${ZSHDDIR}/plugins.zsh"
  for plugin in ${plugins}; do
    if [[ -f "${ZSHDDIR}/plugins/${plugin}.zsh" ]]; then
      source "${ZSHDDIR}/plugins/${plugin}.zsh"
    fi
  done
else
  echo "Please create a plugins.zsh file in ~/.config/zsh.d"
fi

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

eval "$(starship init zsh)"
