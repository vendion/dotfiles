@document.meta
title: Polybar config
description: My polybar configuration.
authors: vendion
created: 2023-03-21
updated: 2023-03-25
version: 1.0.0
tangle: ./config.ini
@end

* About Polybar
  To learn more about how to configure Polybar go to {https://github.com/polybar/polybar}

  The README contains a lot of information Themes : {https://github.com/jaagr/dots/tree/master/.local/etc/themer/themes}
  {https://github.com/jaagr/polybar/wiki/}
  {https://github.com/jaagr/polybar/wiki/Configuration}
  {https://github.com/jaagr/polybar/wiki/Formatting}

  @code dosini
  #                          ___
  #  _   _____  ____  ____/ (_)___  ____   Adam Jimerson (vendion)
  # | | / / _ \/ __ \/ __  / / __ \/ __ \  https://vendion.me
  # | |/ /  __/ / / / /_/ / / /_/ / / / /  https://gitlab.com/vendion
  # |___/\___/_/ /_/\__,_/_/\____/_/ /_/
  #
  #

  @end

* Colors
  @code dosini
  [colors]
  ; Nord theme ============
  background = #282c34
  foreground = #abb2bf
  alert = #bd2c40
  volume-min = #a3be8c
  volume-med = #ebcb8b
  volume-max = #bf616a
  ; =======================

  ; Gotham theme ==========
  ; background = #0a0f14
  ; foreground = #99d1ce
  ; alert = #d26937
  ; volume-min = #2aa889
  ; volume-med = #edb443
  ; volume-max = #c23127
  ; =======================

  @end

* Configuration
  {https://github.com/jaagr/polybar/wiki/Configuration#global-wm-settings}
  {https://github.com/jaagr/polybar/wiki/Configuration#application-settings}
  @code dosini
  [global/wm]
  margin-top = 0
  margin-bottom = 0

  [settings]
  screenchange-reload = true
  pseudo-transparency = true
  compositing-background = over
  compositing-foreground = over
  compositing-overline = over
  compositing-underline = over
  compositing-border = over


  [bar/mybar]
  width = 100%
  height = 24pt
  radius = 0.0
  fixed-center = true

  background = ${colors.background}
  foreground = ${colors.foreground}

  line-size = 3pt

  border-size = 0pt
  border-color = #00000000

  padding-left = 0
  padding-right = 1

  module-margin = 1

  separator = |
  separator-foreground = ${colors.disabled}

  font-0 = GoMono Nerd Font:size=14;2
  font-1 = GoMono Nerd Font:size=16;3
  font-2 = Font Awesome 5 Free:style=Regular;pixelsize=8;1
  font-3 = Font Awesome 5 Free:style=Solid;pixelsize=8;1
  font-4 = Font Awesome 5 Brands:pixelsize=8;1

  modules-left = ewmh
  modules-center = mpd
  modules-right = nordvpn pomod pulseaudio wlan eth battery date

  cursor-click = pointer
  cursor-scroll = ns-resize

  enable-ipc = true

  tray-position = right
  tray-detached = false
  tray-padding = 2
  tray-maxsize = 20
  tray-scale = 1.0
  tray-background = ${colors.background}

  @end

* Modules
  @code dosini
  [module/xworkspaces]
  type = internal/xworkspaces

  label-active = %name%
  label-active-background = ${colors.background-alt}
  label-active-underline= ${colors.primary}
  label-active-padding = 1

  label-occupied = %name%
  label-occupied-padding = 1

  label-urgent = %name%
  label-urgent-background = ${colors.alert}
  label-urgent-padding = 1

  label-empty = %name%
  label-empty-foreground = ${colors.disabled}
  label-empty-padding = 1

  [module/xwindow]
  type = internal/xwindow
  label = %title:0:60:...%

  [module/filesystem]
  type = internal/fs
  interval = 25

  mount-0 = /

  label-mounted = %{F#F0C674}%mountpoint%%{F-} %percentage_used%%

  label-unmounted = %mountpoint% not mounted
  label-unmounted-foreground = ${colors.disabled}

  [module/pulseaudio]
  type = internal/pulseaudio

  format-volume-prefix = "VOL "
  format-volume-prefix-foreground = ${colors.foreground}
  format-volume = <label-volume>

  label-volume = %percentage%%

  label-muted = muted
  label-muted-foreground = ${colors.disabled}

  [module/xkeyboard]
  type = internal/xkeyboard
  blacklist-0 = num lock

  label-layout = %variant%
  label-layout-foreground = ${colors.primary}

  label-indicator-padding = 2
  label-indicator-margin = 1
  label-indicator-foreground = ${colors.background}
  label-indicator-background = ${colors.secondary}

  [module/memory]
  type = internal/memory
  interval = 2
  format-prefix = "RAM "
  format-prefix-foreground = ${colors.primary}
  label = %percentage_used:2%%

  [module/cpu]
  type = internal/cpu
  interval = 2
  format-prefix = "CPU "
  format-prefix-foreground = ${colors.primary}
  label = %percentage:2%%

  [network-base]
  type = internal/network
  interval = 5

  ; Available tokens:
  ;   %ifname%    [wireless+wired]
  ;   %local_ip%  [wireless+wired]
  ;   %essid%     [wireless]
  ;   %signal%    [wireless]
  ;   %upspeed%   [wireless+wired]
  ;   %downspeed% [wireless+wired]
  ;   %linkspeed% [wired]
  ; Default: %ifname% %local_ip%
  label-connected =  %ifname% %linkspeed%
  label-disconnected = %ifname% disconnected

  format-connected-foreground = ${colors.foreground}
  format-connected-background = ${colors.background}
  format-connected-underline = #55aa55
  format-connected-prefix = " "
  format-connected-prefix-foreground = #55aa55
  format-connected-prefix-background = ${colors.background}

  format-disconnected = <label-disconnected>
  format-disconnected-underline = ${colors.alert}
  label-disconnected-foreground = ${colors.foreground}

  [module/wlan]
  inherit = network-base
  interface-type = wireless
  interval = 3.0
  label-connected = %essid%

  ;format-connected = <label-connected>
  format-connected = <ramp-signal> <label-connected>
  format-connected-foreground = ${colors.foreground}
  format-connected-background = ${colors.background}
  format-connected-prefix = "  "
  format-connected-prefix-foreground = #7e52c6
  format-connected-prefix-background = ${colors.background}
  format-connected-underline = #7e52c6

  label-disconnected = %ifname% disconnected
  label-disconnected-foreground = ${colors.alert}
  label-disconnected-background = ${colors.background}

  format-disconnected = <label-disconnected>
  format-disconnected-foreground = ${colors.alert}
  format-disconnected-background = ${colors.background}
  format-disconnected-prefix = "  "
  format-disconnected-prefix-foreground = ${colors.alert}
  format-disconnected-prefix-background = ${colors.background}
  format-disconnected-underline =${colors.alert}

  ramp-signal-0 = ▁
  ramp-signal-1 = ▂
  ramp-signal-2 = ▃
  ramp-signal-3 = ▄
  ramp-signal-4 = ▅
  ramp-signal-5 = ▆
  ramp-signal-6 = ▇
  ramp-signal-7 = █
  ramp-signal-foreground = #7e52c6

  [module/eth]
  inherit = network-base
  interface-type = wired
  label-connected = %{F#F0C674}%ifname%%{F-} %local_ip%

  [module/battery]
  type = internal/battery

  ; This is useful in case the battery never reports 100% charge
  ; Default: 100
  full-at = 99

  ; format-low once this charge percentage is reached
  ; Default: 10
  ; New in version 3.6.0
  low-at = 5

  ; Use the following command to list batteries and adapters:
  ; $ ls -1 /sys/class/power_supply/
  battery = BAT0
  adapter = ADP1

  ; If an inotify event haven't been reported in this many
  ; seconds, manually poll for new values.
  ;
  ; Needed as a fallback for systems that don't report events
  ; on sysfs/procfs.
  ;
  ; Disable polling by setting the interval to 0.
  ;
  ; Default: 5
  poll-interval = 5

  format-charging = <animation-charging> <label-charging>
  label-charging =  %percentage%%
  format-charging-foreground = ${colors.foreground}
  format-charging-background = ${colors.background}
  format-charging-underline = #a3c725

  format-discharging = <ramp-capacity> <label-discharging>
  label-discharging =  %percentage%%
  format-discharging-underline = #c7ae25
  format-discharging-foreground = ${colors.foreground}
  format-discharging-background = ${colors.background}

  format-full-prefix = " "
  format-full-prefix-foreground = #a3c725
  format-full-underline = #a3c725
  format-full-foreground = ${colors.foreground}
  format-full-background = ${colors.background}

  ramp-capacity-0 = 
  ramp-capacity-1 = 
  ramp-capacity-2 = 
  ramp-capacity-3 = 
  ramp-capacity-4 = 
  ramp-capacity-foreground = #c7ae25

  animation-charging-0 = 
  animation-charging-1 = 
  animation-charging-2 = 
  animation-charging-3 = 
  animation-charging-4 = 
  animation-charging-foreground = #a3c725
  animation-charging-framerate = 750

  [module/date]
  type = internal/date
  interval = 1

  date = %l:%M
  date-alt = %Y-%m-%d %l:%M:%S

  label = %date%
  label-foreground = ${colors.primary}

  [module/ewmh]
  type = internal/xworkspaces

  pin-workspaces = false
  enable-click = true
  enable-scroll = true
  reverse-scroll = true

  ;extra icons to choose from
  ;http://fontawesome.io/cheatsheet/
  ;       v     

  icon-0 = 1;
  icon-1 = 2;
  icon-2 = 3;
  icon-3 = 4;
  icon-4 = 5;
  icon-5 = 6;
  icon-6 = 7;
  icon-7 = 8;
  #icon-8 = 9;
  #icon-9 = 10;
  #icon-default = " "
  format = <label-state>
  label-monitor = %name%

  label-active = %name%
  label-active-foreground = ${colors.foreground}
  label-active-background = ${colors.background}
  label-active-underline= #6790eb
  label-active-padding = 1

  label-occupied = %name%
  label-occupied-background = ${colors.background}
  label-occupied-padding = 1

  label-urgent = %name%
  label-urgent-foreground = ${colors.foreground}
  label-urgent-background = ${colors.alert}
  label-urgent-underline = ${colors.alert}
  label-urgent-padding = 1

  label-empty = %name%
  label-empty-foreground = ${colors.foreground}
  label-empty-padding = 1
  format-foreground = ${colors.foreground}
  format-background = ${colors.background}

  [module/mpd]
  type = internal/mpd

  ; Host where mpd is running (either ip or domain name)
  ; Can also be the full path to a unix socket where mpd is running.
  host = /home/vendion/.config/mpd/socket
  ;;port = 6600
  ;;password = mysecretpassword

  ; Seconds to sleep between progressbar/song timer sync
  ; Default: 1
  interval = 2

  ; Available tags:
  ;   <label-song> (default)
  ;   <label-time>
  ;   <bar-progress>
  ;   <toggle> - gets replaced with <icon-(pause|play)>
  ;   <toggle-stop> - gets replaced with <icon-(stop|play)>
  ;   <icon-random>
  ;   <icon-repeat>
  ;   <icon-repeatone> (deprecated)
  ;   <icon-single> - Toggle playing only a single song. Replaces <icon-repeatone>
  ;   <icon-consume>
  ;   <icon-prev>
  ;   <icon-stop>
  ;   <icon-play>
  ;   <icon-pause>
  ;   <icon-next>
  ;   <icon-seekb>
  ;   <icon-seekf>
  format-online = <icon-prev> <icon-seekb> <icon-stop> <toggle> <icon-seekf> <icon-next><label-time>  <label-song>

  ; Available tags:
  ;   <label-offline>
  ; Default: ""
  ;format-offline = <label-offline>

  ; Available tokens:
  ;   %artist%
  ;   %album-artist%
  ;   %album%
  ;   %date%
  ;   %title%
  ; Default: %artist% - %title%
  label-song = 𝄞 %artist% - %title%

  ; Available tokens:
  ;   %elapsed%
  ;   %total%
  ; Default: %elapsed% / %total%
  ;label-time = %elapsed% / %total%

  ; Available tokens:
  ;   None
  label-offline = 🎜 mpd is offline

  ; Only applies if <icon-X> is used
  icon-play = ⏵
  icon-pause = ⏸
  icon-stop = ⏹
  icon-prev = ⏮
  icon-next = ⏭
  icon-seekb = ⏪
  icon-seekf = ⏩
  icon-random = 🔀
  icon-repeat = 🔁
  icon-repeatone = 🔂
  icon-single = 🔂
  icon-consume = ✀

  ; Used to display the state of random/repeat/repeatone/single
  ; Only applies if <icon-[random|repeat|repeatone|single]> is used
  ;toggle-on-foreground = #ff
  ;toggle-off-foreground = #55

  ; Only applies if <bar-progress> is used
  bar-progress-width = 45
  bar-progress-indicator = |
  bar-progress-fill = ─
  bar-progress-empty = ─

  [module/pomod]
  type = custom/script
  exec = /home/vendion/.local/bin/pomod
  tail = true
  format = %{F#f8f8f2}%{u#ff5555}%{+u}<label>%{-u}
  click-left = kill -USR1 %pid%
  click-middle = kill -USR2 %pid%

  [module/protonvpn]
  type = custom/script
  exec = /home/vendion/.local/bin/polybar-protonvpn -o
  tail = true
  click-left = /home/vendion/.local/bin/polybar-protonvpn -t

  [module/nordvpn]
  type = custom/script
  label =  %output%
  format =  <label>
  exec = rofi-nordvpn -s
  interval = 10
  click-left = rofi-nordvpn &
  @end
