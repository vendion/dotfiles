# Based on
# https://github.com/robbyrussell/oh-my-zsh/blob/master/plugins/brew/brew.plugin.zsh

alias brews='brew list -1'
alias bubo='brew update && brew outdated'
alias bubc='brew upgrade && brew cleanup'
alias bubu='bubo && bubc'
