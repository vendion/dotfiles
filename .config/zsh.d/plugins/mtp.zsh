mtp() {
  case "${1}" in
    mount)
      if [[ -n "${2}" ]]; then
        go-mtpfs $2
      else
        echo "Usage $0 [mount|unmount|list] directory\n"
        return 1
      fi
      ;;
    umount)
      if [[ -n "${2}" ]]; then
        fusermount -u $2
      else
        echo "Usage: $0 [mount|unmount|list] directory\n"
        return 1
      fi
      ;;
    list)
      mtp-detect ;;
    *)
      echo "Usage: $0 [mount|unmount|list] directory\n"
      return 1
      ;;
  esac
}
