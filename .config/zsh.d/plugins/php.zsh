switch-php() {
  brew unlink "${1}"
  brew link "${2}"
}

use-php() {
  path=("$(brew --prefix homebrew/php/${1})/bin" "${path[@]}")
}
