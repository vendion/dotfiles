send-to-dropbox() {
  if ! cp "${1}" "${HOME}/Dropbox"; then
    echo "couldn't send ${1} to the dropbox directory" >&2
    return 1
  fi
}
