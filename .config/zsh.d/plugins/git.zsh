# Based on
# https://github.com/robbyrussell/oh-my-zsh/blob/master/plugins/git/git.plugin.zsh

# The current branch name
# Usage example: git pull origin $(current_branch)
# Using '--quiet' with 'symbolic-ref' will ont cause a fatal error (128) if
# it's not a symbalic ref, bit in a Git repo.
current_branch() {
  local ref
  ref="$(git symbolic-ref --quiet HEAD 2> /dev/null)"
  local ret=$?
  if [[ "${ret}" != 0 ]]; then
    [[ "${ret}" == 128 ]] && return # not a git repo.
    ref="$(git rev-parse --short HEAD 2> /dev/null)" || return
  fi
  echo ${ref#refs/heads/}
}

ggl() {
  if [[ "$#" != 0 ]] && [[ "$#" != 1 ]]; then
    git pull origin "${*}"
  else
    [[ "$#" == 0 ]] && local b="$(current_branch)"
    git pull origin "${b:=$1}"
  fi
}
compdef _git ggl=git-checkout

ggp() {
  if [[ "$#" != 0 ]] && [[ "$#" != 1 ]]; then
    git push origin "${*}"
  else
    [[ "$#" == 0 ]] && local b="$(current_branch)"
    git push origin "${b:=$1}"
  fi
}
compdef _git ggp=git-checkout

ggpnp() {
  if [[ "$#" == 0 ]]; then
    ggl && ggp
  else
    ggl "${*}" && ggp "${*}"
  fi
}
compdef _git ggpnp=git-checkout

ggu() {
  [[ "$#" != 1 ]] && local b="$(current_branch)"
  git pull --rebase origin "${b:=$1}"
}
compdef _git ggu=git-checkout

alias ga='git add'
alias gb='git branch'
alias gbl='git blame -b -w'
alias gc='git commit -vs'
alias gc!='git commit -sv --amend'
alias gcb='git checkout -b'
alias gclean='git clean -fd'
alias gcm='git checkout master'
alias gco='git checkout'
alias gd='git diff'
alias gf='git fetch'
alias gfo='git fetch origin'
alias gignore="git update-index --assume-unchanged"
alias gignored='git ls-files -v | grep "^[[:lower:]]"'
alias gunignore="git update-index --no-assume-unchanged"
alias gwch='git whatchanged -p --abbrev-commit --pretty=medium'
alias gwip='git add -A; git rm $(git ls-files --deleted) 2> /dev/null; git commit -m "--wip--"'
alias gunwip='git log -n 1 | grep -q -c "\-\-wip\-\-" && git reset HEAD~1'
alias gsu='git submodule update'
alias gst='git status'
alias glogf="git log --graph --color"
alias ggfetch="git fetch"

function gi() { curl -sL https://www.gitignore.io/api/$@ ;}

_gitignoreio_get_command_list() {
  curl -sL https://www.gitignore.io/api/list | tr "," "\n"
}

_gitignoreio () {
  compset -P '*,'
  compadd -S '' `_gitignoreio_get_command_list`
}

compdef _gitignoreio gi
