if [[ -n ${commands[thefuck]} ]]; then
  eval "$(thefuck --alias)"
fi

if [[ -n ${commands[hub]} ]]; then
  eval "$(hub alias -s)"
fi

if [[ -n ${commands[aura]} ]]; then
  packagesearch() {
    aura -Ss $1; aura -As $1;
  }
fi

if [[ -n "${commands[fzf]}" ]]; then
  if [[ -f "${HOME}/.fzf.zsh" ]]; then
    source "${HOME}/.fzf.zsh"
  fi
fi
