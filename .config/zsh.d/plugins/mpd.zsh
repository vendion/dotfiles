mpd-setup() {
  touch ~/.mpd/{database,log,state,pid,sticker.sql}
}

mtp() {
  case "$1" in
    mount)
      if [[ -n "${2}" ]]; then
        go-mtpfs $2
      else
        echo "Usage $0 [mount|unmount|list]  directory\n"
      fi
      ;;
    unmount)
      if [[ -n "${2}" ]]; then
        fusermount -u $2
      else
        echo "Usage $0 [mount|unmount|list]  directory\n"
      fi
      ;;
    list)
      mtp-detect ;;
    *)
      echo "Usage $0 [mount|unmount|list]  directory\n" ;;
  esac
}

