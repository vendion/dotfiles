alias drushfu='drush fu --version-increment -y '
alias drushca='drush cc all'
alias drushcr='drush cache-rebuild'

rdrush() {
  if [[ "${#@}" == 0 ]]; then
    echo -n "Usage: ${0} pantheon-site pantheon-env command\nFor list of sites and environments run \"drush sa\" for list of commands run \"drush help\"">&2
    return 1
  elif [[ -z "${1}" ]]; then
    echo "Error: no remote site given\nTry running \"drush sa\" for a list of sites\n">&2
    return 1
  elif [[ -z "${2}" ]]; then
    echo "Error: no pantheon environment given\nTry running \"drush sa\" for a list of environments\n">&2
    return 1
  elif [[ -z "${3}" ]]; then
    echo "Error: no command given to run on remote system\nTry running \"drush help\"\n">&2
    return 1
  fi

  drush "@pantheon.${1}.${2}" "${3}"
}
