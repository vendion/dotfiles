# Based on
# https://github.com/robbyrussell/oh-my-zsh/blob/master/plugins/perl/perl.plugin.zsh

if [[ -n "${commands[perlbrew]}" ]]; then
  alias pbi='perlbrew install'
  alias pbl='perlbrew list'
  alias pbo='perlbrew off'
  alias pbs='perlbrew switch'
  alias pbu='perlberw use'
fi

alias latest-perl='curl -s http://www.perl.org/get.html | perl -wlne '\''if (/perl\-([\d\.]+)\.tar\.gz/) { print $1; exit; }'\'
alias pb='perldoc'
alias ple='perl -wlne'

# newpl - creats a basic Perl script file and opens it with $EDITOR
newpl() {
  if [[ -e "${1}" ]]; then
    print "${1} exists; not modifing.\n"
    ${EDITOR} ${1}
  else
    print '#!/usr/bin/env perl'"\n"'use strict;'"\n"'use warnings;'\
      "\n\n" > ${1}
    ${EDITOR} ${1}
  fi
}

# pgs - Perl Global Substitution
# find pattern    = 1st arg
# replace pattern = 2nd arg
# filenam         = 3rd arg
pgs() {
  perl -i.org -pe 's/'"${1}"'/'"${2}"'/g' "${3}"
}

# Perl grep, because 'grep -P' is terrible.  Lets you werk with pipes or files.
prep() { # [pattern] [filename unless STDOUT]
  perl -nle 'print if /'"${1}"'/;' "${2}"
}
