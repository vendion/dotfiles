# Basde on
# https://github.com/robbyrussell/oh-my-zsh/blob/master/plugins/encode64/encode64.plugin.zsh

encode64() {
  echo -n ${1} | base64
}
decode64() {
  echo -n ${1} | base64 --decode
}
alias e64=encode64
alias d64=decode64
