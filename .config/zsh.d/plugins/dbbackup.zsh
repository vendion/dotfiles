dbbackup() {
  if [[ ! -e "./backups"  ]]; then
    "$(mkdir backups)"
  fi

  if [[ -n "${1}" && -n "${2}" && -n "${3}" ]]; then
    date="$(date +%Y%m%d)"
    mkdir "backups/${date}"

    if [[ -n "${4}" ]]; then
      fname="${4}"
    else
      fname="${3}_${date}"
    fi

    "$(mysqldump -u ${1} -p${2} --databases ${3}|xz -e > backups/${date}/${fname}.sql.xz)"
    return 0
  else
    echo "Usage: dbbackup [db user] [db pass] [database] [filename]\n"
  fi
  return 1
}
