send-to-grive() {
  if ! cp "${1}" "${HOME}/Google Drive/"; then
    echo "couldn't send ${1} to Google Drive" >&2
    return 1
  fi
}
