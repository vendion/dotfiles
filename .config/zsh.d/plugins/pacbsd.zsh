# TODO: This should extend the Arch Linux plugin

abs="${HOME}/pacBSD/abs"
buildtop="${HOME}/pacBSD/buildtop/output"
arches=('i386' 'amd64')

# pkgbuild() {
#   if [[ "${EUID}" -ne 0 ]]; then
#     echo "You must be a root user" 2>&1
#     exit 1
#   fi

#   for arch in ${arches}; do
#     pac-build -a ${arch} ${1} ${2}
#   done
#}

pkgsearch() {
  find ${abs} -type d -name ${1}
}

pkgupload() {
  for arch in ${arches}; do
    for file in ${buildtop}/${1}/${arch}/*.tar.xz; do
      repo-send "${file}"
      rm ${file}{,.sig}
    done
  done
}

pkgclean() {
  for repo in "${buildtop}/*"; do
    for arch in "${arches}"; do
      find "${repo}/${arch}" -type f -delete
    done
  done
}
