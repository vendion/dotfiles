went() {
        builtin cd "$(${GOBIN}/went ${@})"
}

if [[ -e "${GOBIN}/went" ]]; then
        alias cd=went
fi
