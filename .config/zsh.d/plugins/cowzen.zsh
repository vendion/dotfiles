cowzen() {
  url="https://api.github.com/zen"

  case "${1}" in
    pony)
      if [[ -e "/usr/bin/ponysay" || -e "/usr/local/bin/ponysay" ]]; then
        curl "${url}" -s | ponysay
      else
        echo "Ponysay does not seem to be installed\n"
      fi
      ;;
    snoo)
      curl "${url}" -s | cowsay -f redditalien ;;
    daemon)
      curl "${url}" -s | cowsay -f daemon ;;
    hellokitty)
      curl "${url}" -s | cowsay -f hellokitty ;;
    tux)
      curl "${url}" -s | cowsay -f tux ;;
    vader)
      curl "${url}" -s | cowsay -f vader ;;
    help)
      echo "Supported options:\npony snoo daemon hellokitty tux vader (null)\n" ;;
    *)
      curl "${url}" -s | cowsay ;;
    esac
}
