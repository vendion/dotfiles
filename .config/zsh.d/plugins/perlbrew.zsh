export PERL_LOCAL_LIB_ROOT="${HOME}/perl5:${PERL_LOCAL_LIB_ROOT}"
export PERL_MB_OPT="--install_base '${HOME}/perl5'"
export PERL_MM_OPT="INSTALL_BASE=${HOME}/perl5"
export PERL5LIB="${HOME}/perl5/lib/perl5:${PERL5LIB}"
path=("${HOME}/perl5/bin" ${path})
eval "$(perl -I${HOME}/perl5/lib/perl5 -Mlocal::lib)"
source "${HOME}/perl5/perlbrew/etc/bashrc"
source "${HOME}/perl5/perlbrew/etc/perlbrew-completion.bash"
