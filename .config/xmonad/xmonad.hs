--                          ___
--  _   _____  ____  ____/ (_)___  ____   Adam Jimerson (vendion)
-- | | / / _ \/ __ \/ __  / / __ \/ __ \  https://vendion.me
-- | |/ /  __/ / / / /_/ / / /_/ / / / /  https://gitlab.com/vendion
-- |___/\___/_/ /_/\__,_/_/\____/_/ /_/
--
--
-- Read README.norg before you dive into my config and change my config.
-- The best way to read README.norg is to see my gitlab at https://gitlab.com/vendion/dotfiles/-/blob/master/.config/xmonad/README.norg

-- Base
import XMonad hiding ( (|||) )
import XMonad.Core
import qualified XMonad.StackSet as W
import System.IO
import System.Exit
import System.Directory

-- Graphics
import Graphics.X11.ExtraTypes.XF86

-- Data
import qualified Data.Map as M
import Data.Monoid
import Data.Maybe
import Data.Semigroup

-- Actions
import XMonad.Actions.NoBorders
import XMonad.Actions.SpawnOn
import XMonad.Actions.UpdatePointer
import XMonad.Actions.Navigation2D
import XMonad.Actions.Promote
import XMonad.Actions.WithAll
import XMonad.Actions.CycleWS

-- Utilities
import XMonad.Util.EZConfig
import XMonad.Util.NamedActions
import XMonad.Util.WorkspaceCompare
import XMonad.Util.NamedScratchpad
import XMonad.Util.SpawnOnce
import XMonad.Util.Run

-- Hooks
import XMonad.Hooks.ToggleHook
import XMonad.Hooks.UrgencyHook
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.ManageHelpers
import XMonad.Hooks.SetWMName
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.WindowSwallowing
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.DynamicProperty

-- Layouts
import XMonad.Layout.Spiral
import XMonad.Layout.Grid
import XMonad.Layout.SimplestFloat
import XMonad.Layout.SimpleFloat
import XMonad.Layout.ThreeColumns
import XMonad.Layout.MultiColumns
import XMonad.Layout.Tabbed

-- Layout modifiers
import XMonad.Layout hiding ( (|||) )
import XMonad.Layout.LayoutCombinators
import XMonad.Layout.Renamed
import XMonad.Layout.Gaps
import XMonad.Layout.LimitWindows
import XMonad.Layout.NoBorders
import qualified XMonad.Layout.ToggleLayouts as T
import XMonad.Layout.Simplest
import XMonad.Layout.SubLayouts
import XMonad.Layout.WindowNavigation
import XMonad.Layout.ToggleLayouts
import XMonad.Layout.LayoutModifier
import XMonad.Layout.Spacing
import XMonad.Layout.ResizableTile
import XMonad.Layout.ShowWName
import qualified XMonad.Layout.MultiToggle as MT (Toggle(..))
import XMonad.Layout.MultiToggle.Instances (StdTransformers(NBFULL, MIRROR, NOBORDERS))

-- ColorScheme
import Colors.Nord

myFont :: String
myFont = "xft:GoMono Nerd Font Mono:regular:size=9:antialias=true:hinting=true"

myTerminal :: String
myTerminal = "kitty"

myEditor :: String
myEditor = myTerminal ++ " nvim "

myLauncher :: String
myLauncher = "rofi -show combi"

myFocusFollowsMouse  :: Bool
myFocusFollowsMouse  = True

myBorderWidth :: Dimension
myBorderWidth = 4

myFocusColor :: String
myFocusColor = color15

myNormColor :: String
myNormColor = colorBack

myModMask :: KeyMask
myModMask = mod4Mask

soundDir :: String
soundDir = "~/.local/share/sound/"
volumeSound :: String
volumeSound = soundDir ++ "ComputerErrorsoundeffect.mp4"

mySoundPlayer :: String
mySoundPlayer = "ffplay -nodisp -autoexit "

myBrowser :: String
myBrowser = "firedragon"

myWorkspaces = ["web", "cmd", "dev", "music", "social", "vm", "dl", "fm", "other", "mail"]
myWorkspaceIndices = M.fromList $ zipWith (,) myWorkspaces [1..] -- (,) == \x y -> (x,y)

clickable ws = "<action=xdotool key super+"++show i++">"++ws++"</action>"
     where i = fromJust $ M.lookup ws myWorkspaceIndices

-- scratchpad = [
--   NS "blueman" "blueman-manager" (className =? "Blueman-manager") defaultFloating,
--   NS "discord" "discord" (className =? "discord") defaultFloating,
--   NS "steam" "steam" (className =? "Steam") defaultFloating,
--   NS "noisetorch" "noisetorch" (className =? "Noisetorch") defaultFloating
--   ] where role = stringProperty "WM_WINDOW_ROLE"

myScratchPads :: [NamedScratchpad]
myScratchPads = [ NS "terminal" spawnTerm findTerm manageTerm
                , NS "gomp" spawnGomp findGomp manageGomp
                , NS "nm" spawnNM findNM manageNM
                , NS "paCtrl" spawnPaCtrl findPaCtrl managePaCtrl
                , NS "noisetorch" spawnNoiseTorch findNoiseTorch manageNoiseTorch
                ]
   where
     spawnTerm  = myTerminal ++ " -T scratchpad"
     findTerm   = title =? "scratchpad"
     manageTerm = customFloating $ W.RationalRect l t w h
                where
                  h = 0.9
                  w = 0.9
                  t = 0.95 -h
                  l = 0.95 -w
     spawnGomp  = myTerminal ++ " -T gomp gomp"
     findGomp   = title =? "gomp"
     manageGomp = customFloating $ W.RationalRect l t w h
                where
                  h = 0.9
                  w = 0.9
                  t = 0.95 -h
                  l = 0.95 -w
     spawnNM  = "nm"
     findNM   = className =? "Nm-connection-editor"
     manageNM = customFloating $ W.RationalRect l t w h
                where
                  h = 0.9
                  w = 0.9
                  t = 0.95 -h
                  l = 0.95 -w
     spawnPaCtrl  = "pavucontrol"
     findPaCtrl   = className =? "Pavucontrol"
     managePaCtrl = customFloating $ W.RationalRect l t w h
                where
                  h = 0.9
                  w = 0.9
                  t = 0.95 -h
                  l = 0.95 -w
     spawnNoiseTorch = "noisetorch"
     findNoiseTorch = title =? "NoiseTorch"
     manageNoiseTorch = customFloating $ W.RationalRect l t w h
                 where
                   h = 0.9
                   w = 0.9
                   t = 0.95 -h
                   l = 0.95 -w

myHandleEventHook :: Event -> X All
myHandleEventHook = dynamicPropertyChange "WM_NAME" (title =? "Spotify" --> doCenterFloat)

myManageHook = composeAll
               [ className =? "confirm"              --> doCenterFloat
               , className =? "file_progress"        --> doCenterFloat
               , className =? "dialog"               --> doCenterFloat
               , className =? "download"             --> doCenterFloat
               , className =? "error"                --> doCenterFloat
               , className =? "Nm-connection-editor" --> doCenterFloat
               , className =? "Gtk2_prefs"           --> doCenterFloat
               , className =? "Steam"                --> doCenterFloat
               , className =? "Pavucontrol"          --> doCenterFloat
               , className =? "Blueman-manager"      --> doCenterFloat
               , className =? "firedragon"           --> doShift (myWorkspaces !! 0)
               , className =? "Brave-browser"        --> doShift (myWorkspaces !! 0)
               , className =? "qutebrowser"          --> doShift (myWorkspaces !! 0)
               , className =? "tabbed"               --> doShift (myWorkspaces !! 2)
               , title     =? "LibreOffice"          --> doShift (myWorkspaces !! 8)
               , className =? "Soffice"              --> doShift (myWorkspaces !! 8)
               , className =? "Steam"                --> doShift (myWorkspaces !! 8)
               , className =? "Keybase"              --> doShift (myWorkspaces !! 4)
               , title     =? "NoiseTorch"           --> doCenterFloat
               , className =? "Yad"                  --> doCenterFloat
               , (className =? "firedragon" <&&> resource =? "Dialog") --> doFloat  -- Float Firefox Dialog
               , isFullscreen -->  doFullFloat
               , className =? "VirtualBox Manager"   --> doShift  ( myWorkspaces !! 5 )
               , title     =? "scratchpad"           --> doCenterFloat
               , title     =? "fm"                   --> doShift (myWorkspaces !! 7)
               , className =? "Bitwarden"            --> doCenterFloat
               ]

mySpacing :: Integer -> l a -> XMonad.Layout.LayoutModifier.ModifiedLayout Spacing l a
mySpacing i = spacingRaw False (Border i i i i) True (Border i i i i) True

-- setting colors for tabs layout and tabs sublayout.
myTabTheme = def { fontName            = myFont
                 , activeColor         = color15
                 , inactiveColor       = color08
                 , activeBorderColor   = color15
                 , inactiveBorderColor = colorBack
                 , activeTextColor     = colorBack
                 , inactiveTextColor   = color16
                 }

-- Theme for showWName which prints current workspace when you change workspaces.
myShowWNameTheme :: SWNConfig
myShowWNameTheme = def
  { swn_font              = "xft:GoMono Nerd Font Mono:bold:size=60"
  , swn_fade              = 1.0
  , swn_bgcolor           = "#1c1f24"
  , swn_color             = "#ffffff"
  }

myLayouts = avoidStruts $
            smartBorders $
            gaps [(U,0), (R,0), (L,0), (D,0)] (
             withBorder myBorderWidth layoutTall
        ||| layoutSpiral
        ||| layoutGrid
        ||| layoutMirror
        ||| layoutFloat
        ||| layoutTreeColumns
        ||| layoutMultiColumns)
    where
      layoutTall =
                 renamed [Replace "Tall"]
                 $ smartBorders
                 $ windowNavigation
                 $ addTabs shrinkText myTabTheme
                 $ subLayout [] (smartBorders Simplest)
                 $ mySpacing 8
                 $ ResizableTall 1 (3/100) (1/2) []
      layoutSpiral =
                 renamed [Replace "Sprial"]
                 $ smartBorders
                 $ windowNavigation
                 $ addTabs shrinkText myTabTheme
                 $ subLayout [] (smartBorders Simplest)
                 $ mySpacing 8
                 $ spiral (6/7)
      layoutGrid =
                 renamed [Replace "Grid"]
                 $ Grid
      layoutMirror =
                 renamed [Replace "Mirror"]
                 $ Mirror (Tall 1 (3/100) (3/5))
      layoutFloat =
                 renamed [Replace "Float"]
                 $ limitWindows 20 simpleFloat
      layoutTreeColumns =
                 renamed [Replace "Treecolumns"]
                 $ ThreeCol 1 (3/100) (1/2)
      layoutMultiColumns =
                 renamed [Replace "Multicolumns"]
                 $ multiCol [1] 1 0.01 (-0.5)

showKeybindings :: [((KeyMask, KeySym), NamedAction)] -> NamedAction
showKeybindings x = addName "Show Keybindings" $ io $ do
  h <- spawnPipe $ "yad --text-info --fontname=\"GoMono Nerd Font 10\" --fore=#f8f8f2 back=#282a36 --center --geometry=800x500 --title \"XMonad keybindings\""
  --hPutStr h (unlines $ showKm x) -- showKM adds ">>" before subtitles
  hPutStr h (unlines $ showKmSimple x) -- showKmSimple doesn't add ">>" to subtitles
  hClose h
  return ()

myKeys conf = let
     subKeys str ks        = subtitle str : mkNamedKeymap conf ks
     in
-- Tips: <mod>/M = Win key/Super
        subKeys "Program keys"
        -- Start Program Launcher
        [ ("M-d", addName "Start Program Launcher"                                     $ spawn myLauncher)
        -- Start Web browser
        , ("M-S-<Tab>", addName "Launch web browser"                                   $ spawn myBrowser)
        -- Start Terminal
        , ("M-<Return>", addName "Launch terminal emulator"                            $ spawn myTerminal)
        -- Start File browser
        , ("M-S-f", addName "Launch file browser"                                      $ spawn (myTerminal ++ " -T fm vifm"))
        -- Start Libreoffice
        , ("M-S-t", addName "Launch Libreoffice. An Microsoft Office open source fork" $ spawn "libreoffice")
        , ("M-S-s", addName "Launch a Kitty Session"                                   $ spawn "${HOME}/.local/bin/kitty-session")
        ]

        ^++^ subKeys "Xmonad Management"
        -- Close window
        [ ("M-S-c", addName "Close Window with focus"                                  $ kill)
        -- Quit xmonad
        , ("M-S-q", addName "Quit Xmonad"                                              $ io (exitWith ExitSuccess))
        -- Restart xmonad
        , ("M-S-r", addName "Restart Xmonad"                                           $ spawn "xmonad --recompile; xmonad --restart")
        -- Show Xmonad config
        --, ("M-S-<Return>", addName "Show Xmonad config"                                $ spawn "emacsclient -c -a 'emacs' ~/.xmonad/README.org")
        ]

        ^++^ subKeys "System Management"
        -- Volume Management
        [ ("<XF86AudioRaiseVolume>", addName "Increase volume"                         $ sequence_ [spawn (mySoundPlayer ++ volumeSound), spawn "${HOME}/.local/bin/changeVolume + 5"])
        , ("<XF86AudioLowerVolume>", addName "Decrease volume"                         $ sequence_ [spawn (mySoundPlayer ++ volumeSound), spawn "${HOME}/.local/bin/changeVolume - 5"])
        , ("<XF86AudioMute>", addName "Mute volume"                                    $ sequence_ [spawn (mySoundPlayer ++ volumeSound), spawn "${HOME}/.local/bin/changeVolume m"])

        -- Brightness Management
        , ("<XF86MonBrightnessUp>", addName "Increase screenlight"                     $ spawn "${HOME}/.local/bin/changeBacklight +")
        , ("<XF86MonBrightnessDown>", addName "Decrease screenlight"                   $ spawn "${HOME}/.local/bin/changeBacklight -")

        -- Lock PC
        , ("M-l", addName "Lock the computer"                                          $ spawn "loginctl lock-sessions")

        -- Take screenshot
        , ("M-p", addName "Take full screen Screenshot"                                $ spawn "scrot -t 20 --delay 5 --count ~/%Y-%m-%d-%T-screenshot.png")
        , ("M-C-p", addName "Take region Screenshot"                                   $ spawn "scrot --select ~/%Y-%m-%d-%T-screenshot.png")

        -- Change background
        , ("M-S-w", addName "Change background"                                        $ spawn "feh --no-fehbg --bg-fill --recursive --randomize ~/Pictures/wallpapers/*")
        ]

        ^++^ subKeys "Scratchpad Program"
      --- Scratchpad
        -- Terminal
        [ ("M-e", addName "Toggle scratchpad terminal"                                 $ namedScratchpadAction myScratchPads "terminal")
        -- Web settings (GUI)
        , ("M-S-n", addName "Toggle scratchpad nm"                                     $ namedScratchpadAction myScratchPads "nm")
        -- NoiseTorch
        , ("M-S-m", addName "Toggle NoiseTorch"                                        $ namedScratchpadAction myScratchPads "noisetorch")
        -- Volume control (GUI)
        -- , ("M-S-l", addName "Launch Pavucontrol. An Gui Volume manager"                $ namedScratchpadAction scratchpad "pavucontrol")
        -- Blueman manager (Bluetooth device manager)
        -- , ("M-b", addName "Launch Bluetooth device GUI manager"                        $ namedScratchpadAction scratchpad "blueman")
        -- Steam
        -- , ("M-c", addName "Launch Steam"                                               $ namedScratchpadAction scratchpad "steam")
        ]

        ^++^ subKeys "Layout"
--- Layout Hotkeys
        [ ("M-C-1", addName "Switch Layout Tall"                                       $ sendMessage $ JumpToLayout "Tall")
        , ("M-C-2", addName "Switch Layout Spiral"                                     $ sendMessage $ JumpToLayout "Sprial")
        , ("M-C-3", addName "Switch Layout Grid"                                       $ sendMessage $ JumpToLayout "Grid")
        , ("M-C-4", addName "Switch Layout Mirror"                                     $ sendMessage $ JumpToLayout "Mirror")
        , ("M-C-5", addName "Switch Layout Float"                                      $ sendMessage $ JumpToLayout "Float")
        , ("M-C-6", addName "Switch Layout Treecolumns"                                $ sendMessage $ JumpToLayout "Treecolumns")
        , ("M-C-7", addName "Switch Layout Multicolumns"                               $ sendMessage $ JumpToLayout "Multicolumns")
        , ("M-w", addName "Sink all windows to the original place"                     $ sinkAll)
        , ("M-C-w", addName "Sink window with focus to the original place"             $ withFocused $ windows . W.sink)
        , ("M-<Tab>", addName "Circle between layout"                                  $ sendMessage NextLayout)
        ]

        ^++^ subKeys "Window Management"
--- Windows
        [ ("M-a", addName "Change focus to master"                                     $ windows W.focusMaster)
        , ("M-j", addName "Change focus down"                                          $ windows W.focusDown)
        , ("M-k", addName "Change focus up"                                            $ windows W.focusUp)
        , ("M-S-j", addName "Change window place down"                                 $ windows W.swapDown)
        , ("M-S-k", addName "Change window place up"                                   $ windows W.swapUp)
        , ("M-<Space>", addName "Change window place master"                           $ promote)
        , ("M-u", addName "Change window size -"                                       $ sendMessage Shrink)
        , ("M-i", addName "Change window size +"                                       $ sendMessage Expand)
        , ("M-f", addName "Make window fullscreen"                                     $ sendMessage (MT.Toggle NBFULL) >> sendMessage ToggleStruts)
        ]

        ^++^ subKeys "Workspaces Management"
--- Workspaces
        [ ("M-<Right>", addName "Move to next Workspace"                               $ nextWS)
        , ("M-<Left>", addName "Move to prev Workspace"                                $ prevWS)
        , ("M-1", addName "Switch to workspace 1"                                      $ (windows $ W.greedyView $ myWorkspaces !! 0))
        , ("M-2", addName "Switch to workspace 2"                                      $ (windows $ W.greedyView $ myWorkspaces !! 1))
        , ("M-3", addName "Switch to workspace 3"                                      $ (windows $ W.greedyView $ myWorkspaces !! 2))
        , ("M-4", addName "Switch to workspace 4"                                      $ (windows $ W.greedyView $ myWorkspaces !! 3))
        , ("M-5", addName "Switch to workspace 5"                                      $ (windows $ W.greedyView $ myWorkspaces !! 4))
        , ("M-6", addName "Switch to workspace 6"                                      $ (windows $ W.greedyView $ myWorkspaces !! 5))
        , ("M-7", addName "Switch to workspace 7"                                      $ (windows $ W.greedyView $ myWorkspaces !! 6))
        , ("M-8", addName "Switch to workspace 8"                                      $ (windows $ W.greedyView $ myWorkspaces !! 7))
        , ("M-9", addName "Switch to workspace 9"                                      $ (windows $ W.greedyView $ myWorkspaces !! 8))
        , ("M-0", addName "Switch to workspace 10"                                      $ (windows $ W.greedyView $ myWorkspaces !! 9))
        , ("M-S-1", addName "Send to workspace 1"                                      $ (windows $ W.shift $ myWorkspaces !! 0))
        , ("M-S-2", addName "Send to workspace 2"                                      $ (windows $ W.shift $ myWorkspaces !! 1))
        , ("M-S-3", addName "Send to workspace 3"                                      $ (windows $ W.shift $ myWorkspaces !! 2))
        , ("M-S-4", addName "Send to workspace 4"                                      $ (windows $ W.shift $ myWorkspaces !! 3))
        , ("M-S-5", addName "Send to workspace 5"                                      $ (windows $ W.shift $ myWorkspaces !! 4))
        , ("M-S-6", addName "Send to workspace 6"                                      $ (windows $ W.shift $ myWorkspaces !! 5))
        , ("M-S-7", addName "Send to workspace 7"                                      $ (windows $ W.shift $ myWorkspaces !! 6))
        , ("M-S-8", addName "Send to workspace 8"                                      $ (windows $ W.shift $ myWorkspaces !! 7))
        , ("M-S-9", addName "Send to workspace 9"                                      $ (windows $ W.shift $ myWorkspaces !! 8))
        , ("M-S-0", addName "Send to workspace 10"                                     $ (windows $ W.shift $ myWorkspaces !! 9))
        ]

        ^++^ subKeys "Screen Management"
--- Screen Management
        [ ("M-S-<Right>", addName "Move window with focus to next Screen"              $ shiftNextScreen)
        , ("M-S-<Left>", addName "Move window with focus to prev Screen"               $ shiftPrevScreen)
        , ("M-<Up>", addName "Move to next screen"                                     $ nextScreen)
        , ("M-<Down>", addName "Move to prev screen"                                   $ prevScreen)
        ]

--- Mouse ---
myMouseBindings (XConfig {XMonad.modMask = mod}) = M.fromList $

    -- mod-button1, Set the window to floating mode and move by dragging
    [ ((mod, button1), (\w -> focus w >> mouseMoveWindow w
                                       >> windows W.shiftMaster))

    -- mod-button2, Raise the window to the top of the stack
    , ((mod, button2), (\w -> focus w >> windows W.shiftMaster))

    -- mod-button3, Set the window to floating mode and resize by dragging
    , ((mod, button3), (\w -> focus w >> mouseResizeWindow w
                                       >> windows W.shiftMaster))

    ]

myStartupHook :: X ()
myStartupHook = do
                setWMName "xmonad"
                spawn "killall picom"
                spawnOnce "${HOME}/.local/bin/prime-config-xmonad default"
                spawnOnce "~/.fehbg"
                spawnOnce "dunst"
                spawn ("sleep 2 && picom")
                spawnOnce "lxsession"
                spawnOnce "nm-applet"
                spawnOnce "kwalletd5"
                spawnOnce ("trayer --edge top --align right --widthtype request --padding 6 --SetDockType true --SetPartialStrut true --expand true --monitor 0 --transparent true --alpha 0 " ++ colorTrayer ++ " --height 22")
                spawnOnce "blueman-applet"
                spawnOnce "udiskie --no-tray"
                spawnOnce "run_keybase"
                spawnOnce "/usr/lib/kdeconnectd"
                spawnOnce "xss-lock lock"
                spawnOnce "aa-notify -p -s 1 -w 60 -f /var/log/audit/audit.log"
                spawnOnce "conky"
                spawnOnce "protonmail-bridge --no-window"

main :: IO ()
main = do
  xmproc <- spawnPipe "xmobar -x 0 ~/.config/xmobar/xmobarrc"
  xmonad $ docks
         $ ewmhFullscreen
         $ withUrgencyHook NoUrgencyHook
         $ defaults {
             logHook = dynamicLogWithPP $  filterOutWsPP [scratchpadWorkspaceTag] $ xmobarPP
             { ppOutput = hPutStrLn xmproc
                , ppCurrent = xmobarColor color06 "" . wrap
                    ("<box type=Bottom width=2 mb=2 color=" ++ color06 ++ ">") "</box>"
                -- Visible but not current workspace
                , ppVisible = xmobarColor color06 "" . clickable
                -- Hidden workspace
                , ppHidden = xmobarColor color05 "" . wrap
                     ("<box type=Top width=2 mt=2 color=" ++ color05 ++ ">") "</box>" . clickable
                -- Hidden workspaces (no windows)
                , ppHiddenNoWindows = xmobarColor color05 ""  . clickable
                -- Title of active window
                , ppTitle = xmobarColor color16 "" . shorten 60
                -- Separator character
                , ppSep =  "<fc=" ++ color09 ++ "> <fn=1>|</fn> </fc>"
                -- Urgent workspace
                , ppUrgent = xmobarColor color02 "" . wrap "!" "!"
                -- order of things in xmobar
                , ppOrder  = \(ws:l:t:ex) -> [ws,l]++ex++[t]
             }
       }

defaults = addDescrKeys' ((mod4Mask, xK_s), showKeybindings) myKeys def {
      -- simple stuff
        focusFollowsMouse  = myFocusFollowsMouse,
        handleEventHook    = myHandleEventHook,
        borderWidth        = myBorderWidth,
        modMask            = myModMask,
        terminal           = myTerminal,
        -- numlockMask        = myNumlockMask,
        workspaces         = myWorkspaces,

      -- key bindings
        mouseBindings      = myMouseBindings,

        -- hooks, layouts
        layoutHook         = showWName' myShowWNameTheme $ myLayouts,
        normalBorderColor  = myNormColor,
        focusedBorderColor = myFocusColor,
        startupHook        = myStartupHook,
        manageHook         = myManageHook
    }
