local vo = vim.opt_local
vo.tabstop = 4
vo.shiftwidth = 4
vo.softtabstop = 4

local wk = require("which-key")
local default_options = { silent = true }
wk.register({
  c = {
    name = "Coding",
    a = { "<cmd>GoAddTag<cr>", "Add tags to struct" },
    c = { "<cmd>GoCoverage<cr>", "Test coverage" },
    e = { "<cmd>GoIfErr<cr>", "Add if err" },
    g = { "<cmd>lua require('go.comment').gen()<cr>", "Generate comment" },
    l = { "<cmd>GoLint<cr>", "Run linter" },
    r = { "<cmd>GoRun<cr>", "Run" },
    s = { "<cmd>GoFillStruct<cr>", "Autofill struct" },
    t = { "<cmd>GoTest<cr>", "Run tests" },
    v = { "<cmd>GoVet<cr>", "Go vet" },
  },
  t = {
    name = "Tests",
    n = { "<cmd>TestNearest<CR>", "Test the nearest function" },
    f = { "<cmd>TestFile<CR>", "Test file" },
    s = { "<cmd>TestSuite<CR>", "Run all tests/test framework" },
    l = { "<cmd>TestLast<CR>", "Runs the last test" },
    g = { "<cmd>TestVisit<CR>", "Vists the test file from which the last test was ran from" },
  },
}, {
  prefix = "<LocalLeader>",
  mode = "n",
  default_options,
})

vim.cmd([[
  autocmd BufWritePre (InsertLeave?) <buffer> lua vim.lsp.buf.formatting_sync(nil,500)
]])

--vim.g["test#strategy"] = "dispatch"
vim.g["test#strategy"] = "kitty"

-- Run gofmt + goimport on save
local format_sync_grp = vim.api.nvim_create_augroup("GoImport", {})
vim.api.nvim_create_autocmd("BufWritePre", {
  pattern = "*.go",
  callback = function()
    require("go.format").goimport()
  end,
  group = format_sync_grp,
})
