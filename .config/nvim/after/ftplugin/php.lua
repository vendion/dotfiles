vim.keymap.set("n", "<Leader>pm", "<cmd>PhpactorContextMenu<CR>")
vim.keymap.set("n", "<Leader>pn", "<cmd>PhpactorClassNew<CR>")
--[[ vim.keymap.set("n", "<buffer><localleader>u", "<cmd>PhpactorImportClass<CR>") ]]
--[[ vim.keymap.set("n", "<buffer><localleader>e", "<cmd>PhpactorClassExpand<CR>") ]]
