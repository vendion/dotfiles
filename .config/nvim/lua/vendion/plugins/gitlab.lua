return {
  "git@gitlab.com:gitlab-org/editor-extensions/gitlab.vim.git",
  event = { "BufReadPre", "BufNewFile" },
  ft = { "go", "javascript" },
  cond = function()
    return vim.env.GITLAB_TOKEN ~= nil and vim.env.GITLAB_TOKEN ~= ""
  end,
  keys = {
    { "<C-g>", "<Plug>(GitLabToggleCodeSuggestions)", desc = "Toggle GitLab Code Suggestions" },
  },
  build = "npm install",
  opts = {
    statusline = {
      enabled = true,
    },
    code_suggestions = {
      offset_encoding = "utf-16",
    },
  },
}
