return {
  "nvimtools/none-ls.nvim", -- configure formatters & linters
  lazy = true,
  event = { "BufReadPre", "BufNewFile" }, -- to enable uncomment this
  dependencies = {
    "jay-babu/mason-null-ls.nvim",
  },
  config = function()
    local mason_null_ls = require("mason-null-ls")
    local null_ls = require("null-ls")
    local null_ls_utils = require("null-ls.utils")

    mason_null_ls.setup({
      ensure_installed = {
        "ansiblelint",
        "blade_formatter",
        "codespell",
        "commitlint",
        "editorconfig_checker",
        "eslint_d", -- js linter
        "gitlint",
        "golangci_lint",
        "jsonlint",
        "luacheck",
        "markdownlint",
        "perlimports",
        "perltidy",
        "php",
        "phpcs",
        "phpcbf",
        "phpcsfixer",
        "phpmd",
        "prettier",
        "shellcheck",
        "shfmt",
        "stylelint",
        "stylua",
        "tidy",
        "todo_comments",
        "trail_space",
      },
    })

    -- for conciseness
    local code_actions = null_ls.builtins.code_actions
    local diagnostics = null_ls.builtins.diagnostics -- to setup linters
    local formatting = null_ls.builtins.formatting -- to setup formatters

    -- to setup format on save
    local augroup = vim.api.nvim_create_augroup("LspFormatting", {})

    -- configure null_ls
    null_ls.setup({
      diagnostics_format = "[#{s}] #{m} #{c}",
      -- add package.json as identifier for root (for typescript monorepos)
      root_dir = null_ls_utils.root_pattern(".null-ls-root", "Makefile", ".git", "package.json"),
      -- setup formatters & linters
      sources = {
        -- Code Actions
        code_actions.shellcheck,
        -- Diagnostic/Linters
        diagnostics.ansiblelint,
        diagnostics.codespell,
        diagnostics.commitlint,
        diagnostics.editorconfig_checker,
        diagnostics.eslint_d.with({ -- js/ts linter
          condition = function(utils)
            return utils.root_has_file({ ".eslintrc.js", ".eslintrc.cjs" })
          end,
        }),
        diagnostics.gitlint,
        diagnostics.golangci_lint,
        diagnostics.jsonlint,
        diagnostics.luacheck,
        diagnostics.markdownlint,
        diagnostics.php,
        diagnostics.phpcs,
        diagnostics.phpmd.with({ extra_args = { "cleancode,codesize,controversial,design,naming,unusedcode" } }),
        diagnostics.stylelint,
        diagnostics.tidy,
        diagnostics.todo_comments,
        diagnostics.trail_space.with({ disabled_filetypes = { "NvimTree", "gitcommit" } }),
        -- Formatters
        formatting.blade_formatter,
        formatting.perlimports,
        formatting.perltidy,
        -- formatting.phpcbf,
        formatting.phpcsfixer,
        formatting.prettier.with({
          extra_filetypes = { "gotexttmpl" },
        }),
        formatting.shfmt,
        formatting.stylua, -- lua formatter
      },
      -- configure format on save
      on_attach = function(current_client, bufnr)
        if current_client.supports_method("textDocument/formatting") then
          vim.api.nvim_clear_autocmds({ group = augroup, buffer = bufnr })
          vim.api.nvim_create_autocmd("BufWritePre", {
            group = augroup,
            buffer = bufnr,
            callback = function()
              vim.lsp.buf.format({
                filter = function(client)
                  --  only use null-ls for formatting instead of lsp server
                  return client.name == "null-ls"
                end,
                bufnr = bufnr,
              })
            end,
          })
        end
      end,
    })
  end,
}
