return {
  "nvimdev/lspsaga.nvim",
  event = "LspAttach",
  config = true,
  dependencies = {
    "nvim-treesitter/nvim-treesitter",
    "nvim-tree/nvim-web-devicons",
  },
}
