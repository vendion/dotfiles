return {
  "neovim/nvim-lspconfig",
  event = { "BufReadPre", "BufNewFile" },
  dependencies = {
    "hrsh7th/cmp-nvim-lsp",
    { "antosha417/nvim-lsp-file-operations", config = true },
    "nvimdev/lspsaga.nvim",
  },
  config = function()
    local lspconfig = require("lspconfig")
    local cmp_nvim_lsp = require("cmp_nvim_lsp")
    local keymap = vim.keymap

    local opts = { noremap = true, silent = true }
    local on_attach = function(client, bufnr)
      opts.buffer = bufnr

      -- set keybinds
      opts.desc = "Format code with LSP"
      keymap.set("n", "<leader>f", vim.lsp.buf.format, opts)

      opts.desc = "Show LSP references"
      keymap.set("n", "gR", "<cmd>Lspsaga finder ref<CR>", opts) -- show definition, references

      opts.desc = "Go to definition"
      keymap.set("n", "gd", "<cmd>Lspsaga goto_definition<CR>", opts) -- show lsp definitions

      opts.desc = "Show definition"
      keymap.set("n", "<leader>pd", "<cmd>Lspsaga peek_definition<CR>", opts) -- show definition")

      opts.desc = "Go to type declaration"
      keymap.set("n", "gD", "<cmd>Lspsaga goto_type_definition", opts) -- go to declaration

      opts.desc = "Show type definition"
      keymap.set("n", "<leader>pD", "<cmd>Lspsaga peek_type_definition<CR>", opts) -- show definition")

      opts.desc = "Show LSP implementations"
      keymap.set("n", "gi", "<cmd>Lspsaga finder imp<CR>", opts)

      opts.desc = "See available code actions"
      keymap.set({ "n", "v" }, "<leader>ca", "<cmd>Lspsaga code_action<CR>", opts) -- see available code actions, in visual mode will apply to selection

      opts.desc = "Smart rename"
      keymap.set("n", "<leader>rn", "<cmd>Lspsaga rename<CR>", opts) -- smart rename

      opts.desc = "Show documentation for what is under cursor"
      keymap.set("n", "K", "<cmd>Lspsaga hover_doc<CR>", opts) -- show documentation for what is under cursor

      opts.desc = "Restart LSP"
      keymap.set("n", "<leader>rs", "<cmd>LspRestart<CR>", opts) -- mapping to restart lsp if necessary
    end

    -- used to enable autocompletion (assign to every lsp server config)
    local capabilities = cmp_nvim_lsp.default_capabilities()

    -- Change the Diagnostic symbols in the sign column (gutter)
    -- (not in youtube nvim video)
    local signs = { Error = " ", Warn = " ", Hint = "󰠠 ", Info = " " }
    for type, icon in pairs(signs) do
      local hl = "DiagnosticSign" .. type
      vim.fn.sign_define(hl, { text = icon, texthl = hl, numhl = "" })
    end

    -- configure html server
    lspconfig["html"].setup({
      capabilities = capabilities,
      on_attach = on_attach,
      filetypes = { "html", "gotexttmpl" },
    })

    lspconfig["htmx"].setup({
      capabilities = capabilities,
      on_attach = on_attach,
      filetypes = { "html", "gotexttmpl" },
    })

    -- configure css server
    lspconfig["cssls"].setup({
      capabilities = capabilities,
      on_attach = on_attach,
    })

    -- configure tailwindcss server
    lspconfig["tailwindcss"].setup({
      capabilities = capabilities,
      on_attach = on_attach,
      filetypes = {
        "aspnetcorerazor",
        "astro",
        "astro-markdown",
        "blade",
        "clojure",
        "django-html",
        "htmldjango",
        "edge",
        "eelixir",
        "elixir",
        "ejs",
        "erb",
        "eruby",
        "gohtml",
        "gohtmltmpl",
        "haml",
        "handlebars",
        "hbs",
        "html",
        "html-eex",
        "heex",
        "jade",
        "leaf",
        "liquid",
        "markdown",
        "mdx",
        "mustache",
        "njk",
        "nunjucks",
        "php",
        "razor",
        "slim",
        "twig",
        "css",
        "less",
        "postcss",
        "sass",
        "scss",
        "stylus",
        "sugarss",
        "javascript",
        "javascriptreact",
        "reason",
        "rescript",
        "typescript",
        "typescriptreact",
        "vue",
        "svelte",
        "gotexttmpl",
      },
    })

    -- configure lua server (with special settings)
    lspconfig["lua_ls"].setup({
      capabilities = capabilities,
      on_attach = on_attach,
      settings = { -- custom settings for lua
        Lua = {
          -- make the language server recognize "vim" global
          diagnostics = {
            globals = { "vim" },
          },
          workspace = {
            -- make language server aware of runtime files
            library = {
              [vim.fn.expand("$VIMRUNTIME/lua")] = true,
              [vim.fn.stdpath("config") .. "/lua"] = true,
            },
          },
        },
      },
    })

    -- configure gopls server
    lspconfig["gopls"].setup({
      capabilities = capabilities,
      on_attach = on_attach,
      settings = {
        gopls = {
          analyses = {
            nilness = true,
            unusedparams = true,
            unusedwrite = true,
            useany = true,
          },
          experimentalPostfixCompletions = true,
          gofumpt = true,
          staticcheck = true,
          usePlaceholders = true,
          hints = {
            assignVariableTypes = true,
            compositeLiteralFields = true,
            compositeLiteralTypes = true,
            constantValues = true,
            functionTypeParameters = true,
            parameterNames = true,
            rangeVariableTypes = true,
          },
        },
      },
    })

    -- configure hls server
    lspconfig["hls"].setup({
      capabilities = capabilities,
      on_attach = on_attach,
      filetypes = { "haskell", "lhaskell", "cabal" },
    })

    -- configure phpactor server
    lspconfig["phpactor"].setup({
      capabilities = capabilities,
      on_attach = on_attach,
      cmd = { os.getenv("HOME") .. "/.local/share/nvim/lazy/phpactor/bin/phpactor", "language-server" },
    })

    -- configure jsonls server
    lspconfig["jsonls"].setup({
      capabilities = capabilities,
      on_attach = on_attach,
      settings = {
        json = {
          schemas = require("schemastore").json.schemas(),
          validate = { enable = true },
        },
      },
    })

    -- vim.lsp.set_log_level("debug")
    -- configure perlnavigator server
    lspconfig["perlnavigator"].setup({
      capabilities = capabilities,
      on_attach = on_attach,
      cmd = { os.getenv("HOME") .. "/.local/share/nvim/mason/bin/perlnavigator" },
      settings = {
        perlnavigator = {
          logging = true,
          perlPath = "perl",
          enableWarnings = true,
          perltidyProfile = "$workspaceFolder/.perltidyrc",
          perlcriticEnabled = true,
          perlcriticProfile = "$workspaceFolder/.perlcriticrc",
          perlimportsLintEnabled = true,
          perlimportsTidyEnabled = true,
          perlimportsProfile = "",
        },
      },
    })
  end,
}
