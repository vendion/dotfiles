return {
  "folke/trouble.nvim",
  dependencies = { "nvim-tree/nvim-web-devicons" },
  opts = {},
  keys = {
    {
      "<leader>xx",
      function()
        require("trouble").toggle()
      end,
      desc = "Toggle trouble",
    },
    {
      "[d",
      function()
        require("trouble").previous({ sxip_groups = true, jump = true })
      end,
      desc = "Go to previous diagnostic",
    },
    {
      "]d",
      function()
        require("trouble").next({ sxip_groups = true, jump = true })
      end,
      desc = "Go to next diagnostic",
    },
  },
}
