return {
  {
    "akinsho/toggleterm.nvim",
    version = "*",
    opts = {
      open_mapping = [[<c-\>]],
      direction = "float",
      shell = "elvish",
      float_opts = {
        border = "curved",
      },
    },
  },
}
