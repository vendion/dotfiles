return {
  "akinsho/bufferline.nvim",
  dependencies = {
    "nvim-tree/nvim-web-devicons",
    "shaunsingh/nord.nvim",
  },
  version = "*",
  opts = {
    options = {
      indicator = {
        icon = " ",
      },
      show_close_icon = false,
      tab_size = 0,
      max_name_length = 25,
      offsets = {
        {
          filetype = "NvimTree",
          text = "  Files",
          highlight = "StatusLine",
          text_align = "left",
        },
      },
      separator_style = "slant",
      custom_areas = {
        left = function()
          return {
            { text = "    ", fg = "#8fff6d" },
          }
        end,
      },
      highlights = {
        fill = {
          bg = { attribute = "bg", highlight = "StatusLine" },
        },
        background = {
          bg = { attribute = "bg", highlight = "StatusLine" },
        },
        tab = {
          bg = { attribute = "bg", highlight = "StatusLine" },
        },
        tab_close = {
          bg = { attribute = "bg", highlight = "StatusLine" },
        },
        close_button = {
          bg = { attribute = "bg", highlight = "StatusLine" },
          fg = { attribute = "fg", highlight = "StatusLineNonText" },
        },
        close_button_visible = {
          bg = { attribute = "bg", highlight = "StatusLine" },
          fg = { attribute = "fg", highlight = "StatusLineNonText" },
        },
        close_button_selected = {
          fg = { attribute = "fg", highlight = "StatusLineNonText" },
        },
        buffer_visible = {
          bg = { attribute = "bg", highlight = "StatusLine" },
        },
        modified = {
          bg = { attribute = "bg", highlight = "StatusLine" },
        },
        modified_visible = {
          bg = { attribute = "bg", highlight = "StatusLine" },
        },
        duplicate = {
          bg = { attribute = "bg", highlight = "StatusLine" },
        },
        duplicate_visible = {
          bg = { attribute = "bg", highlight = "StatusLine" },
        },
        separator = {
          fg = { attribute = "bg", highlight = "StatusLine" },
          bg = { attribute = "bg", highlight = "StatusLine" },
        },
        separator_selected = {
          fg = { attribute = "bg", highlight = "StatusLine" },
          bg = { attribute = "bg", highlight = "Normal" },
        },
        separator_visible = {
          fg = { attribute = "bg", highlight = "StatusLine" },
          bg = { attribute = "bg", highlight = "StatusLine" },
        },
      },
      groups = {
        options = {
          toggle_hidden_on_enter = true,
        },
        items = {
          {
            name = "Tests",
            highlight = { underline = true, sp = "blue" },
            priority = 2,
            icon = "",
            matcher = function(buf)
              return buf.path:match("%_test")
                or buf.path:match("%_spec")
                or buf.path:match("%.input")
                or buf.path:match("%.golden")
            end,
          },
          {
            name = "Docs",
            highlight = { undercurl = true, sp = "green" },
            auto_close = false, -- whether or not close this group if it doesn't contain the current buffer
            matcher = function(buf)
              return buf.path:match("%.md") or buf.path:match("%.txt") or buf.path:match("README.norg")
            end,
            --[[ separator = { -- Optional ]]
            --[[ style = require("bufferline.groups").separator.tab, ]]
            --[[ }, ]]
          },
        },
      },
    },
  },
}
