return {
  "codota/tabnine-nvim",
  enabled = function()
    if vim.loop.os_uname().sysname == "Linux" then
      return true
    end

    return false
  end,
  build = "./dl_binaries.sh",
  config = function()
    require("tabnine").setup({})

    vim.keymap.set({ "n", "x" }, "<leader>tc", function()
      require("tabnine.chat").open()
    end, { noremap = true, desc = "Tabnine Chat" })
  end,
}
