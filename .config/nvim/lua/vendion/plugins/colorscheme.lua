-- URL: https://github.com/shaunsingh/nord.nvim

return {
  {
    "shaunsingh/nord.nvim",
    lazy = false,
    priority = 1000,
    config = function()
      vim.cmd([[colorscheme nord]])
      vim.api.nvim_create_autocmd({ "ColorScheme" }, {
        pattern = "nord",
        group = vim.api.nvim_create_augroup("config", { clear = true }),
        callback = function()
          vim.api.nvim_set_hl(0, "Normal", { bg = "none" })
          vim.api.nvim_set_hl(0, "NormalFloat", { bg = "none" })
          vim.api.nvim_set_hl(0, "BufferLineFill", {
            bg = vim.api.nvim_get_hl_by_name("StatusLine", true).background,
          })
          vim.api.nvim_set_hl(0, "BufferLineSeparator", {
            bg = vim.api.nvim_get_hl_by_name("StatusLine", true).background,
          })
          vim.api.nvim_set_hl(0, "BufferLineSeparatorVisible", {
            bg = vim.api.nvim_get_hl_by_name("StatusLine", true).background,
          })
          vim.api.nvim_set_hl(0, "StatusLineNonText", {
            fg = vim.api.nvim_get_hl_by_name("NonText", true).foreground,
            bg = vim.api.nvim_get_hl_by_name("StatusLine", true).background,
          })
        end,
      })
    end,
  },
  -- {
  --   "nyngwang/nvimgelion",
  --   config = function()
  --     -- do whatever you want for further customization~
  --   end,
  -- }
}
