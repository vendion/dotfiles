return {
  {
    "nvim-neorg/neorg",
    cmd = "Neorg",
    ft = "norg",
    build = ":Neorg sync-parsers",
    version = "*",
    opts = {
      load = {
        ["core.defaults"] = {}, -- Loads default behaviour
        ["core.concealer"] = {
          config = {
            icon_preset = "diamond",
          },
        }, -- Adds pretty icons to your documents
        ["core.dirman"] = { -- Manages Neorg workspaces
          config = {
            workspaces = {
              notes = "~/.local/share/neorg/notes",
              work = "~/.local/share/neorg/work",
            },
            autodetect = true,
            autochdir = true,
            default_workspace = "notes",
          },
        },
        ["core.completion"] = {
          config = {
            engine = "nvim-cmp",
          },
        },
        ["core.export"] = {},
        ["core.export.markdown"] = {},
        ["core.presenter"] = {
          config = {
            zen_mode = "zen-mode",
          },
        },
        ["core.esupports.metagen"] = {
          config = {
            type = "auto",
          },
        },
        ["core.journal"] = {
          config = {
            workspace = "notes",
          },
        },
        ["core.summary"] = {},
        --[[ ["core.ui.calendar"] = {}, ]]
        --[[ ["core.tempus"] = {}, ]]
      },
    },
    dependencies = { { "nvim-lua/plenary.nvim" } },
  },
}
