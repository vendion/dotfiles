return {
  {
    "fgheng/winbar.nvim",
    opts = {
      enabled = true,
      show_file_path = true,
      show_symbols = true,
      icons = {
        file_icon_default = "",
        seperator = ">",
        editor_state = "●",
        lock_icon = "",
      },
      exclude_filetype = {
        "help",
        "startify",
        "dashboard",
        "neogitstatus",
        "NvimTree",
        "Trouble",
        "alpha",
        "lir",
        "Outline",
        "spectre_panel",
        "toggleterm",
        "qf",
      },
    },
    dependencies = {
      { "nvim-tree/nvim-web-devicons" },
      { "SmiteshP/nvim-navic" },
    },
  },
}
