return {
  {
    "rhysd/vim-grammarous",
    cmd = "GrammarousCheck",
    config = function()
      vim.g["grammarous#jar_url"] = "https://www.languagetool.org/download/LanguageTool-5.9.zip"
      --[[ vim.g["grammarous#use_vim_spelllang"] = 1 ]]
      vim.g["grammarous#use_location_list"] = 1
    end,
  },
}
