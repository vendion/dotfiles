return {
  {
    "ray-x/go.nvim",
    ft = { "go", "gomod" },
    event = { "CmdlineEnter" },
    opts = {
      goimport = "gopls",
      gofmt = "gopls",
      max_line_length = 120,
      tag_transform = false,
      test_dir = "",
      lsp_cfg = false,
      lsp_on_attach = false,
      lsp_gofumpt = true,
      dap_debug = false,
      dap_debug_keymap = false,
      luasnip = true,
    },
    dependencies = {
      { "ray-x/guihua.lua" },
      { "vim-test/vim-test" },
      { "neovim/nvim-lspconfig" },
      { "nvim-treesitter/nvim-treesitter" },
    },
    build = ':lua require("go.install").update_all_sync()',
  },
}
