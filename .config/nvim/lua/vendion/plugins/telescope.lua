return {
  "nvim-telescope/telescope.nvim",
  branch = "0.1.x",
  dependencies = {
    "nvim-lua/plenary.nvim",
    "nvim-lua/popup.nvim",
    "BurntSushi/ripgrep",
    { "sharkdp/fd", lazy = true },
    { "nvim-telescope/telescope-fzf-native.nvim", build = "make" },
    "nvim-tree/nvim-web-devicons",
    "crispgm/telescope-heading.nvim",
    "nvim-telescope/telescope-symbols.nvim",
    "nvim-telescope/telescope-file-browser.nvim",
    "nvim-telescope/telescope-ui-select.nvim",
  },
  config = function()
    local telescope = require("telescope")
    local actions = require("telescope.actions")
    local action_layout = require("telescope.actions.layout")
    local builtin = require("telescope.builtin")
    local fb_actions = telescope.extensions.file_browser.actions
    local trouble = require("trouble.providers.telescope")

    vim.cmd([[
    highlight link TelescopePromptTitle PMenuSel
    highlight link TelescopePrevviewTitle PMenuSel
    highlight link TelescopePromptNormal NarmalFloat
    highlight link TelescopePromptBorder FloatBorder
    highlight link TelescopeNormal  CursorLine
    highlight link TelescopeBorder CursorLineBg
]])

    telescope.setup({
      extensions = {
        fzf = {
          fuzzy = true,
          override_generic_sorter = true,
          override_file_sorter = true,
          case_mode = "smart_case",
        },
        ["ui-select"] = {
          require("telescope.themes").get_dropdown({}),
        },
        file_browser = {
          mappings = {
            i = {
              ["<C-n>"] = fb_actions.create,
              ["<C-r>"] = fb_actions.rename,
              ["<C-h>"] = fb_actions.toggle_hidden,
              ["<C-x>"] = fb_actions.remove,
              ["<C-p>"] = fb_actions.move,
              ["<C-y>"] = fb_actions.copy,
              ["<C-a>"] = fb_actions.select_all,
            },
          },
        },
      },
      pickers = {
        find_files = {
          hidden = true,
        },
        buffers = {
          ignore_current_buffer = true,
          sort_lastused = true,
        },
      },
      defaults = {
        file_sorter = require("telescope.sorters").get_fzy_sorter,
        path_display = { "truncate " },
        mappings = {
          i = {
            ["<C-k>"] = actions.move_selection_previous, -- move to prev result
            ["<C-j>"] = actions.move_selection_next, -- move to next result
            ["<C-q>"] = actions.send_selected_to_qflist + actions.open_qflist,
            ["<C-t>"] = trouble.open_with_trouble,
          },
          n = {
            ["<C-t>"] = trouble.open_with_trouble,
          },
        },
      },
    })

    telescope.load_extension("fzf")
    telescope.load_extension("heading")
    telescope.load_extension("file_browser")
    telescope.load_extension("ui-select")
    -- telescope.load_extension("notify")

    -- set keymaps
    local keymap = vim.keymap -- for conciseness

    keymap.set("n", "<leader>ff", builtin.find_files, { desc = "Fuzzy find files in cwd" })
    keymap.set("n", "<leader>fg", builtin.git_files, { desc = "Fuzzy git files in cwd" })
    keymap.set("n", "<leader>fr", builtin.oldfiles, { desc = "Fuzzy find recent files" })
    keymap.set("n", "<leader>fs", builtin.live_grep, { desc = "Find string in cwd" })
    keymap.set("n", "<leader>fc", builtin.grep_string, { desc = "Find string under cursor in cwd" })
    keymap.set("n", "<leader>fb", builtin.buffers, { desc = "Fuzzy find open buffer" })
    keymap.set("n", "<leader>fm", builtin.man_pages, { desc = "Fuzzy find man pages" })
    keymap.set("n", "<leader>fh", builtin.help_tags, { desc = "Fuzzy find help tags" })
  end,
}
