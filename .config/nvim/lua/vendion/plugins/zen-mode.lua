return {
  "folke/zen-mode.nvim",
  cmd = "ZenMode",
  opts = {
    window = {
      width = 240,
      foldcolumn = 0,
    },
  },
  dependencies = {
    { "folke/twilight.nvim" },
  },
}
