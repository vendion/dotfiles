return {
  {
    "phpactor/phpactor",
    version = "2023-06-17-2",
    build = "composer install --no-dev --optimize-autoloader",
    ft = "php",
  },
}
