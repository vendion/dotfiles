return {
  {
    "nvim-treesitter/nvim-treesitter",
    event = { "BufReadPre", "BufNewFile" },
    build = ":TSUpdate",
    dependencies = {
      "nvim-treesitter/nvim-treesitter-context",
      "mfussenegger/nvim-ts-hint-textobject",
      "RRethy/nvim-treesitter-endwise",
      "nvim-treesitter/nvim-treesitter-textobjects",
      "windwp/nvim-ts-autotag",
      {
        "LiadOz/nvim-dap-repl-highlights",
        config = true,
      },
    },
    config = function()
      -- import nvim-treesitter plugin
      local treesitter = require("nvim-treesitter.configs")

      require("nvim-dap-repl-highlights").setup()
      -- configure treesitter
      treesitter.setup({ -- enable syntax highlighting
        highlight = {
          enable = true,
          additional_vim_regex_highlighting = { "php", "markdown" },
        },
        -- enable indentation
        indent = { enable = true },
        -- enable autotagging (w/ nvim-ts-autotag plugin)
        autotag = {
          enable = true,
        },
        -- ensure these language parsers are installed
        ensure_installed = {
          "bash",
          "comment",
          "css",
          "dart",
          "elvish",
          "gitignore",
          "go",
          "gomod",
          "gowork",
          "html",
          "javascript",
          "json",
          "lua",
          "make",
          "markdown",
          "markdown_inline",
          "perl",
          "php",
          "phpdoc",
          "regex",
          "scss",
          "sql",
          "toml",
          "query",
          "vim",
          "yaml",
        },
        incremental_selection = {
          enable = true,
          keymaps = {
            init_selection = "<C-space>",
            node_incremental = "<C-space>",
            scope_incremental = false,
            node_decremental = "<bs>",
          },
        },
        endwise = { enable = true },
        autopairs = { enable = true },
      })

      require("nvim-autopairs").setup({
        check_ts = true, -- use treesitter to check for a pair.
        ts_config = {
          lua = { "string", "source" }, -- it will not add pair on that treesitter node
          javascript = { "string", "template_string" },
        },
        disable_filetype = { "TelescopePrompt" },
        enable_check_bracket_line = true, -- Don't add pairs if it already has a close pair in the same line
        ignored_next_char = "[%w%.]", -- will ignore alphanumeric and `.` symbol
        fast_wrap = {
          map = "<M-e>",
          chars = { "{", "[", "(", '"', "'" },
          pattern = string.gsub([[ [%'%"%)%>%]%)%}%,] ]], "%s+", ""),
          offset = 0, -- Offset from pattern match
          end_key = "$",
          keys = "qwertyuiopzxcvbnmasdfghjkl",
          check_comma = true,
          highlight = "PmenuSel",
          highlight_grey = "LineNr",
        },
      })

      local cmp_autopairs = require("nvim-autopairs.completion.cmp")
      local cmp_status_ok, cmp = pcall(require, "cmp")
      if not cmp_status_ok then
        return
      end
      cmp.event:on("confirm_done", cmp_autopairs.on_confirm_done({ map_char = { tex = "" } }))
    end,
  },
}
