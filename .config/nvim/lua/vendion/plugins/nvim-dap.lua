return {
  {
    "mfussenegger/nvim-dap",
    dependencies = {
      { "rcarriga/nvim-dap-ui" },
      { "theHamsta/nvim-dap-virtual-text" },
      { "nvim-telescope/telescope-dap.nvim" },
      { "leoluz/nvim-dap-go" },
      {
        "LiadOz/nvim-dap-repl-highlights",
        config = true,
      },
    },
    lazy = true,
    keys = {
      {
        "<leader>B",
        function()
          require("dap").toggle_breakpoint()
        end,
        desc = "Toggle breakpoint",
      },
      {
        "<leader>bb",
        function()
          require("dapui").toggle()
        end,
        desc = "Toggle debugging UI",
      },
      {
        "<leader>bn",
        function()
          require("dap").continue()
        end,
        desc = "Start/Continue debugging",
      },
      {
        "<leader>bq",
        function()
          require("dap").terminate()
        end,
        desc = "Stop debugging",
      },
      {
        "<leader>bl",
        function()
          require("dap").step_over()
        end,
        desc = "Step over",
      },
      {
        "<leader>bj",
        function()
          require("dap").step_into()
        end,
        desc = "Step into",
      },
      {
        "<leader>bk",
        function()
          require("dap").step_out()
        end,
        desc = "Step out",
      },
      {
        "<leader>bh",
        function()
          require("dap").step_back()
        end,
        desc = "Step back",
      },
      {
        "<leader>b.",
        function()
          require("dap").run_last()
        end,
        desc = "Repeat last command",
      },
      {
        "<leader>br",
        function()
          require("dap").repl.open()
        end,
        desc = "Open REPL",
      },
      {
        "<leader>be",
        function()
          require("dap").eval()
        end,
        desc = "Open Eval",
      },
      {
        "<leader>bE",
        function()
          require("dapui").eval(vim.fn.input("[DAP] Expression > "))
        end,
        desc = "Open Eval Expression",
      },
      {
        "<leader>bt",
        function()
          require("dap-go").debug_test()
        end,
        desc = "Debug tests",
      },
      {
        "<leader>bh",
        function()
          require("dap.ui.widgets").hover()
        end,
      },
      {
        "<leader>bp",
        function()
          require("dap.ui.widgets").preview()
        end,
      },
      {
        "<leader>bf",
        function()
          local widgets = require("dap.ui.widgets")
          widgets.centered_float(widgets.frames)
        end,
      },
      {
        "<leader>bs",
        function()
          local widgets = require("dap.ui.widgets")
          widgets.centered_float(widgets.scopes)
        end,
      },
    },
    config = function()
      local dap = require("dap")
      local dapui = require("dapui")

      vim.fn.sign_define("DapBreakpoint", { text = "ß", texthl = "", linehl = "", numhl = "" })
      vim.fn.sign_define("DapBreakpointCondition", { text = "ü", texthl = "", linehl = "", numhl = "" })
      -- Setup cool Among Us as avatar
      vim.fn.sign_define("DapStopped", { text = "ඞ", texthl = "Error" })

      require("nvim-dap-virtual-text").setup({
        enabled = true,
        -- DapVirtualTextEnable, DapVirtualTextDisable, DapVirtualTextToggle, DapVirtualTextForceRefresh
        enabled_commands = false,
        -- highlight changed values with NvimDapVirtualTextChanged, else always NvimDapVirtualText
        highlight_changed_variables = true,
        highlight_new_as_changed = true,
        -- prefix virtual text with comment string
        commented = false,
        show_stop_reason = true,
        -- experimental features:
        virt_text_pos = "eol", -- position of virtual text, see `:h nvim_buf_set_extmark()`
        all_frames = false, -- show virtual text for all stack frames not only current. Only works for debugpy on my machine.
      })

      -- vim.keymap.set("n", "<leader>dB", function()
      --   dap.set_breakpoint(vim.fn.input("[DAP] Condition > "))
      -- end, { desc = "Toggle breakpoint (Conditional)" })

      -- You can set trigger characters OR it will default to '.'
      -- You can also trigger with the omnifunc, <c-x><c-o>
      vim.cmd([[
augroup DapRepl
  au!
  au FileType dap-repl lua require('dap.ext.autocompl').attach()
augroup END
]])

      require("dap-go").setup()

      dap.adapters.php = {
        type = "executable",
        command = "node",
        args = { os.getenv("HOME") .. "/.local/share/vscode-php-debug/out/phpDebug.js" },
      }

      dap.configurations.php = {
        {
          type = "php",
          request = "launch",
          name = "Listen for Xdebug",
          port = 9003,
          pathMappings = {
            ["/opt/autowall"] = "${workspaceFolder}",
          },
        },
      }

      dapui.setup()

      dap.listeners.after.event_initialized["dapui_config"] = function()
        dapui.open()
      end
      dap.listeners.before.event_terminated["dapui_config"] = function()
        dapui.close()
      end
      dap.listeners.before.event_exited["dapui_config"] = function()
        dapui.close()
      end

      require("nvim-dap-repl-highlights").setup()
    end,
  },
}
