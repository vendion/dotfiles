return {
  "RRethy/vim-illuminate",
  config = function()
    require("illuminate").configure({
      delay = 500,
      filetypes_denylist = {
        "dirvish",
        "fugitive",
        "NvimTree",
        "alpha",
      },
    })
  end,
}
