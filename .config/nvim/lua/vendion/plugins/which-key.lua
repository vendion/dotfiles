return {
  "folke/which-key.nvim",
  event = "VeryLazy",
  init = function()
    vim.o.timeout = true
    vim.o.timeoutlen = 500
  end,
  opts = {
        plugins = {
            spelling = {
                enabled = true, -- enabling this will show WhichKey when pressing z= to select spelling suggestions
                suggestions = 20, -- how many suggestions should be shown in the list
            },
            presets = {
                operatiors = true, -- adds help for operators like d, y, ... and registers them for motion / text object completion
                motions = true,
                text_objects = true,
                windows = true,
                nav = true,
                z = true,
                g = true,
            },
        },
        operators = { gc = "Comments" },
        window = {
            border = "shadow",
            position = "bottom",
            margin = { 1, 0, 1, 0 },
            padding = { 2, 2, 2, 2 },
        },
        layout = {
            align = "center",
        },
        hidden = {
            "<silent>",
            "<cmd>",
            "<Cmd>",
            "<cr>",
            "<CR>",
            "call",
            "lua",
            "require",
            "^:",
            "^ ",
        },
        triggers_blacklist = {
            i = { "j", "k" },
            v = { "j", "k" },
        },
    },
}
