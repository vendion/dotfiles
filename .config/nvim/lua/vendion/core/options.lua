local opt = vim.opt

-- File Encoding
opt.encoding = "utf-8"

-- Cursor
opt.guicursor = ""

-- Line Numbers
opt.number = true
opt.relativenumber = true

-- Tabs & Indentiation
opt.tabstop = 4
opt.softtabstop = 4
opt.shiftwidth = 4
opt.expandtab = true
opt.smartindent = true

-- Line Wrapping
opt.wrap = false

-- Swap File & Backups
opt.swapfile = false
opt.backup = false
opt.undodir = os.getenv("HOME") .. "/.vim/undodir"
opt.undofile = true

-- Highlighting
opt.incsearch = true
opt.ignorecase = true
opt.smartcase = true

-- Appearance
opt.termguicolors = true
opt.background = "dark"
opt.signcolumn = "yes:2"
opt.isfname:append("@-@")
opt.colorcolumn = "120"
opt.laststatus = 3
opt.title = true
opt.mouse = "a"
opt.confirm = true

-- Backspace
opt.backspace = "indent,eol,start"

-- Clipboard
opt.clipboard:append("unnamedplus")

-- Folds
opt.foldmethod = "expr"
opt.foldexpr = "nvim_treesitter#foldexpr()"

-- Lists
opt.list = true
opt.listchars = "eol:¬,tab:>·,trail:~,extends:>,precedes:<"
opt.fillchars:append({ eob = " " })

-- OmniCompletion
opt.wildmode = "longest:full,full"
opt.completeopt = "menu,menuone,longest,preview"
