use git

var symbol-for = [&ahead="↑" &behind="↓" &diverged="⥄ " &dirty="⨯" &none="◦"]

fn prompt {
  edit:styled (tilde-abbr $pwd) 33
  if (git:is-repo) {
    put ' on '; edit:styled (git:branch-name) 32

    if (git:is-touched) {
      put ' '$symbol-for[dirty]' '
    } else {
      put ' '$symbol-for[(git:ahead)]' '
    }
  } else {
    put ' ❱ '
  }
}

