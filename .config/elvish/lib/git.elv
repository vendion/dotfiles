fn current-branch {
  var ref = (git symbolic-ref --quiet HEAD 2> /dev/null)
  var @parts = (splits &sep="/" $ref)
  echo $parts[2]
}

fn pull {|@a|
  if (!= (count $@a) 0) {
    if (!= (count $@a) 1) {
      git pull origin $@a
    }
  } else {
    if (== (count $@a) 0) {
      var branch = (current-branch)
      git pull origin $branch
    }
  }
}

fn push {|@a|
  if (!= (count $@a) 0) {
    if (!= (count $@a) 1) {
      git push origin $@a
    }
  } else {
    if (== (count $@a) 0) {
      var branch = (current-branch)
      git push origin $branch
    }
  }
}

fn update {|@a|
  if (== (count $@a) 0) {
    pull
    push
  } else {
    pull $@a
    push $@a
  }
}

fn rebase-pull {|@a|
  if (!= (count $@a) 1) {
    var branch = (current-branch)
    git pull --rebase origin $branch
  }
}

fn clean {
  var branch = (current-branch)
  git checkout $branch
}

fn is-detached {
  not ?(git symbolic-ref --short HEAD >/dev/null 2>&1)
}

fn is-repo {
  if (not ?(test -d .git)) {
    ?(git rev-parse --dir >/dev/null 2>&1)
  }
}

fn is-touched {
  test -n (echo (git status --porcelain 2>&1))
}

fn branch-name {
  try {
    git symbolic-ref --short HEAD 2>/dev/null
  } except {
    git show-ref --head -s --abbrev | head -n1
  }
}

fn commit-count {
  splits &sep="\t" (git rev-list --count --left-right "@{upstream}...HEAD" 2>/dev/null)
}

fn ahead {
  if (is-detached) {
    put detached; return
  }

  var commits-behind = 0
  var commits-ahead = 0
  try {
    set commits-behind commits-ahead = (commit-count)
  } except {
    put none; return
  }

  if (== $commits-behind 0) {
    if (== $commits-ahead 0) {
      put none
    } else {
      put ahead
    }
  } else {
    if (== $commits-ahead 0) {
      put behind
    } else {
      put diverged
    }
  }
}

fn a {|@a| git add $@a }
fn b {|@a| git branch $@a }
fn blame {|@a| git blame -b -w $@a }
fn commit {|@a| git commit -sv $@a }
fn ammend {|@a| git commit -sv --amend $@a }
fn create-branch {|@a| git checkout -b $@a }
fn co {|@a| git checkout $@a }
fn ignore {|@a| git update-index --assume-unchanged $@a }
fn unignore {|@a| git update-index --no-assume-unchanged $@a }
fn whatchanged {|@a| git whatchanged -p --abbrev-comit --pretty=medium }
fn sub-update {|@a| git submodule update $@a }
fn log {|@a| git log --graph --color }
fn create-gitignore {|lang| curl -sL https://www.gitignore.io/api/$lang }
fn list-gitignore {
  var @gi = (curl -sL https://www.gitignore.io/api/list | tr "," "\n")
  echo $gi
}
