fn fu {
  drush features-update --version-increment -y $args
}

fn ca {
  drush cache-clear all
}

fn cr {
  drush cache-rebuild
}

fn enable {
  drush pm-enable -y $args
}
