use path
use str

var bwDir = $E:HOME/.local/share/bw
var masterPass = $bwDir/masterpass

fn savepasswd {|pass|
  if (not (path:is-dir $bwDir)) {
    mkdir $bwDir
  }

  echo $pass > $masterPass
  chmod go-r $masterPass
}

fn unlock {
  var o = (bw unlock --passwordfile $masterPass | grep 'export BW_SESSION' | awk '{print $3}')
  var a b = (str:split &max=2 = $o)
  set-env BW_SESSION $b
}

fn lock {
  bw lock
}

fn list {|x &search=''|
  var i = (bw list $x --search $search | from-json)
  #pprint $i
  each {|item|
    #pprint $item
    printf "ID:\t%s\nName:\t%s\n" $item[id] $item[name]

    if (has-key $item login) {
        printf "Usorname:\t%s\n" $item[login][username]
    }

    if (has-key $item[login] uris) {
        each {|uri|
          printf "URL:\t%s\n" $uri[uri]
        } $item[login][uris]
    }

    printf "\n"
  } $i
}
