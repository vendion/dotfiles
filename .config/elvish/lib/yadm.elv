fn push {|@a|
  if (== (count $@a) 0) {
    e:yadm push origin master
  } else {
    e:yadm push origin $@a[1]
  }
}

fn pull {|@a|
  if (== (count $@a) 0) {
    e:yadm pull origin master
  } else {
    e:yadm pull origin $@a[1]
  }
}

fn encrypt {|f|
  if (!= $f "") {
    echo $f >> $E:HOME/.config/yadm/encrypt
    e:yadm encrypt
  }
}
