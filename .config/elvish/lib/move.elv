fn rmv {|@a|
  rsync -v --progress --remove-source-files $@a
}
