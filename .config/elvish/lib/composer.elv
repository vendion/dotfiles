fn update {
  composer self-update
}

fn update {
  composer global update
}

fn require {
  composer global require
}

fn autoload {
  composer dump-autoload
}
