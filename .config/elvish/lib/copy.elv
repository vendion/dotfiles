fn rcp {|@a|
  rsync -v --progress $@a
}

fn cpv {|@a|
  rsync -poghb --backup-dir=/tmp/rsync -e /dev/null --progress -- $@a
}
