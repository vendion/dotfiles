fn proxy_on {|&host="127.0.0.1" &username="" &pass=""|
        set-env no_proxy "localhost,127.0.0.1,localaddress,.localdomain.com"
        set-env NO_PROXY "localhost,127.0.0.1,localaddress,.localdomain.com"


        pre=""
        if ?(eq $username "") {
                pre=$username":"$pass"@"
        }

        proxy="http://"$pre$host"/"
        set-env http_proxy $proxy
        set-env https_proxy $proxy
        set-env ftp_proxy $proxy
        set-env rsync_proxy $proxy
        set-env HTTP_PROXY $proxy
        set-env HTTPS_PROXY $proxy
        set-env FTP_PROXY $proxy
        set-env RSYNC_PROXY $proxy
}

fn proxy_off {
        unset-env http_proxy
        unset-env https_proxy
        unset-env ftp_proxy
        unset-env rsync_proxy
        unset-env HTTP_PROXY
        unset-env HTTPS_PROXY
        unset-env FTP_PROXY
        unset-env RSYNC_PROXY
}
