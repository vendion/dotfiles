fn newpl {|name|
  if ([[ -e $name ]]) {
    echo "$name exists; not modifing."
  } else {
    echo '$!/usr/bin/env perl' > $name
    echo 'use strict;' >> $name
    echo 'use warnings;' >> $name
    echo "\n\n" >> $name
    $E:EDITOR $name
  }
}
