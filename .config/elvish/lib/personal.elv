use platform

# Environment Variables
set-env GOPROXY https://proxy.golang.org
set-env SSH_AUTH_SOCK $E:XDG_RUNTIME_DIR'/ssh-agent.socket'
if (eq $E:XDG_SESSION_TYPE "wayland") {
  set-env MOZ_ENABLE_WAYLAND 1
}
if (eq $platform:os "linux") {
  if (eq (platform:hostname &strip-domain=$true) "Vili") {
    set-env LIBVA_DRIVER_NAME vdpau
    set-env DEBUGINFOD_URLS "https://debuginfod.archlinux.org/"
  }
}
set-env CHROME_EXECUTABLE 'google-chrome-stable'
set-env MPD_PATH "~/.config/mpd/socket"

# Alias Definitions
use github.com/zzamboni/elvish-modules/alias
if (eq $platform:os "freebsd") {
  alias:new ls e:ls -FG
} else {
  alias:new ls e:ls --color=always --classify
  if (eq (platform:hostname &strip-domain=$true) "Vili") {
    alias:new pacman yay
    alias:new openfortivpn e:openfortivpn --config ~/.config/openfortivpn/config
  }
}
alias:new hc herbstclient
alias:new sudo doas
alias:new sudoedit doas nvim
alias:new gartisan go run . artisan
alias:new vim nvim

# Custom functions
use path
fn t {|x|
  cd (path:temp-dir $x'-')
}

use kt

if (path:is-regular $E:HOME'/.config/elvish/lib/secrets.elv') {
  use secrets
}

eval (carapace _carapace|slurp)
