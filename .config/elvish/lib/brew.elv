fn installed {
  brew list -l
}

fn outdated {
  brew update
  println "\t== Outdated =="
  brew outdated
}

fn upgrade {
  brew upgrade
  brew cleanup
}

fn upgrade-clean {
  outdated
  upgrade
}
