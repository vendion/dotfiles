# Plugin for kitty term

fn session {|x|
  e:kitty --session $E:HOME/.config/kitty/sessions/$x.conf &
}
