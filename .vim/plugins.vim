"""""""""""""""""""""""""""""""""""""""
"                                     "
"          Plugins Setup              "
"                                     "
"""""""""""""""""""""""""""""""""""""""

""""""""""""""""""""""""""""""
"                            "
"     Plugin overrides       "
"                            "
""""""""""""""""""""""""""""""
let g:ale_disable_lsp = 1 " The ALE docs say this should be set before loading plugnis
                          " See https://github.com/dense-analysis/ale#faq-coc-nvim

""""""""""""""""""""""""""""""
"                            "
"         vim-plug           "
"                            "
""""""""""""""""""""""""""""""
let data_dir = has('nvim') ? stdpath('data') . '/site' : '~/.vim'
if empty(glob(data_dir . '/autoload/plug.vim'))
  silent execute '!curl -fLo '.data_dir.'/autoload/plug.vim --create-dirs  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin()

""""""""""""""""""""""""""""""
"                            "
"   Themes & Display         "
"                            "
""""""""""""""""""""""""""""""
Plug 'whatyouhide/vim-gotham'
Plug 'vim-airline/vim-airline' | Plug 'vim-airline/vim-airline-themes'
Plug 'Konfekt/FastFold'
Plug 'ryanoasis/vim-devicons'
Plug 'rrethy/vim-hexokinase', { 'do': 'make hexokinase' }
Plug 'arcticicestudio/nord-vim'

""""""""""""""""""""""""""""""
"                            "
" Auto Complete & LSP        "
"                            "
""""""""""""""""""""""""""""""
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'honza/vim-snippets'

""""""""""""""""""""""""""""""
"                            "
"    Navigation & Search     "
"                            "
""""""""""""""""""""""""""""""
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'
Plug 'jremmen/vim-ripgrep'
Plug 'scrooloose/nerdtree' | Plug 'Xuyuanp/nerdtree-git-plugin' | Plug 'tiagofumo/vim-nerdtree-syntax-highlight'
Plug 'ctrlpvim/ctrlp.vim'

""""""""""""""""""""""""""""""
"                            "
"          Syntax            "
"                            "
""""""""""""""""""""""""""""""
Plug 'sheerun/vim-polyglot'
Plug 'pearofducks/ansible-vim'
Plug 'cakebaker/scss-syntax.vim', { 'on_ft': ['scss']}
Plug 'dmix/elvish.vim', { 'on_ft': ['elvish']}
Plug 'pangloss/vim-javascript', { 'on_ft': ['js']}
Plug 'chrisbra/csv.vim', { 'on_ft': ['csv']}
Plug 'elzr/vim-json', { 'on_ft': ['json']}
Plug 'jwalton512/vim-blade'

""""""""""""""""""""""""""""""
"                            "
"         Utilities          "
"                            "
""""""""""""""""""""""""""""""
Plug 'editorconfig/editorconfig-vim'
Plug 'tpope/vim-fugitive' | Plug 'shumphrey/fugitive-gitlab.vim' | Plug 'https://git.sr.ht/~willdurand/srht.vim' | Plug 'tpope/vim-rhubarb'
Plug 'AndrewRadev/splitjoin.vim'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-eunuch'
Plug 'tommcdo/vim-exchange'
Plug 'tpope/vim-obsession'
Plug 'tpope/vim-repeat'
Plug 'kshenoy/vim-signature'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-vinegar'
Plug 'dense-analysis/ale'
Plug 'antonk52/vim-tabber'
Plug 'voldikss/vim-floaterm'
Plug 'godlygeek/tabular'
Plug 'vim-utils/vim-man'
Plug 'mbbill/undotree'
Plug 'stsewd/fzf-checkout.vim'
Plug 'psliwka/vim-dirtytalk', { 'do': ':DirtytalkUpdate' }
Plug 'vimwiki/vimwiki'
Plug 'mhinz/vim-startify'
Plug 'aaronbieber/vim-quicktask'
Plug 'itchyny/calendar.vim'

""""""""""""""""""""""""""""""
"                            "
"            PHP             "
"                            "
""""""""""""""""""""""""""""""
Plug 'noahfrederick/vim-laravel' | Plug 'noahfrederick/vim-composer' | Plug 'tpope/vim-projectionist' | Plug 'tpope/vim-dispatch'
Plug 'phpactor/phpactor', {'for': 'php', 'branch': 'master', 'do': 'composer install --no-dev -o'} | Plug 'camilledejoye/phpactor-mappings'
Plug 'tobyS/pdv' | Plug 'tobyS/vmustache' | Plug 'SirVer/ultisnips'

""""""""""""""""""""""""""""""
"                            "
"         Markdown           "
"                            "
""""""""""""""""""""""""""""""
Plug 'davinche/godown-vim'
Plug 'plasticboy/vim-markdown'

""""""""""""""""""""""""""""""
"                            "
"             Go             "
"                            "
""""""""""""""""""""""""""""""
Plug 'govim/govim' | Plug 'buoto/gotests-vim' | Plug 'sebdah/vim-delve'

""""""""""""""""""""""""""""""
"                            "
"             Go             "
"                            "
""""""""""""""""""""""""""""""
Plug 'https://tildegit.org/sloum/gemini-vim-syntax'

" Initialize plugin system
call plug#end()
