" -------------------------------------------------------------------------------------------------
" godown.vim default settings
" -------------------------------------------------------------------------------------------------

" should the preview be shown automatically when a markdown buffer is opened
let g:godown_autorun = 0

" the port to run the Godown server on
let g:godown_port = 1337
