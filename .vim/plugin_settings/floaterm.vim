" -------------------------------------------------------------------------------------------------
" floaterm default settings
" -------------------------------------------------------------------------------------------------

let g:floaterm_keymap_toggle = '<LocalLeader>tt'
let g:floaterm_keymap_next   = '<LocalLeader>tn'
let g:floaterm_keymap_prev   = '<LocalLeader>tp'
let g:floaterm_keymap_new    = '<LocalLeader>ft'

let g:floaterm_shell = 'elvish'
let g:floaterm_gitcommit='floaterm'
let g:floaterm_autoinsert=1
let g:floaterm_width=0.8
let g:floaterm_height=0.8
let g:floaterm_wintitle=0
