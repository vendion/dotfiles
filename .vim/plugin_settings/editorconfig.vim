" -------------------------------------------------------------------------------------------------
" Editorconfig default settings
" -------------------------------------------------------------------------------------------------

" Ignore remote files and Fugitive
let g:EditorConfig_exclude_patterns = ['fugitive://.*', 'scp://.*']
"
let OS=substitute(system('uname -s'),"\n","","")
if (OS == "FreeBSD")
  let g:EditorConfig_exec_path = '/usr/local/bin/editorconfig'
else
  let g:EditorConfig_exec_path = '/usr/bin/editorconfig'
endif
