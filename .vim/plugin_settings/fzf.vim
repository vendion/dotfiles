" -------------------------------------------------------------------------------------------------
" fzf default settings
" -------------------------------------------------------------------------------------------------

let g:fzf_layout = { 'up': '~90%', 'window': { 'width': 0.8, 'height': 0.8, 'yoffset':0.5, 'xoffset': 0.5 } }
let $FZF_DEFAULT_OPTS = '--layout=reverse --info=inline --reverse'

nmap <Leader>ff :Files<CR>
nmap <Leader>faf :AllFiles<CR>
nmap <Leader>fb :Buffers<CR>
nmap <Leader>fh :History<CR>
nmap <Leader>fgs :GFiles<CR>
nmap <Leader>fgb :GBranches<CR>
