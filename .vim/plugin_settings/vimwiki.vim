" -------------------------------------------------------------------------------------------------
"  VimWiki settings
" -------------------------------------------------------------------------------------------------

let g:vimwiki_list = [{'path': '~/.local/share/vimwiki/',
                      \ 'template_path': '~/.local/share/vimwiki/templates/',
                      \ 'path_html': '~/.local/share/vimwiki/site_html/', 'custom_wiki2html': 'vimwiki_markdown',
                      \ 'html_filename_parameterization': 1,
                      \ 'syntax': 'markdown', 'ext': '.md'}]

let g:vimwiki_use_mouse  =  1
let g:vimwiki_camel_case  =  0
let g:vimwiki_menu = ''
let g:vimwiki_folding = 'expr:quick'
let g:vimwiki_CJK_length = 1
let g:vimwiki_hl_cb_checked = 1
let g:vimwiki_table_auto_fmt = 0
