" -------------------------------------------------------------------------------------------------
" ALE default settings
" -------------------------------------------------------------------------------------------------

" Control when linters run
let g:ale_lint_on_enter = 1
let g:ale_lint_on_save = 1
let g:ale_lint_on_text_changed = 0
let g:ale_lint_on_filetype_changed = 1

" Control when fixers run
let g:ale_fix_on_save = 1

" Set custome error and warning flgas
let g:ale_echo_msg_error_str = 'E'
let g:ale_echo_msg_warning_str = 'W'
let g:ale_echo_msg_format = '[%linter%] %s [%severity%]'

" Global fixers definition
let g:ale_fixers = {
\   '*': ['remove_trailing_lines', 'trim_whitespace'],
\}
