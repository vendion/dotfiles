" -------------------------------------------------------------------------------------------------
" vim-airline default settings
" -------------------------------------------------------------------------------------------------

let g:airline_theme='nord'
let g:airline_powerline_fonts = 1
let g:airline_skip_empty_sections = 1

let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#left_sep = ' '
let g:airline#extensions#tabline#left_alt_sep = '|'
let g:airline#extensions#tabline#formatter = 'unique_tail_improved'
