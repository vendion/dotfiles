" General PHP settings
setlocal tabstop=4 shiftwidth=4 expandtab

let php_folding = 2
let php_htmlInStrings = 1
let php_sql_query = 1
let php_noShortTags = 1
let php_baselib = 1

let b:ale_linters = ['php', 'phpcs', 'phpmd', 'psalm']
let b:ale_fixers = ['phpcbf', 'php_cs_fixer']

let g:ale_php_phpcs_standard = "PSR12"
let g:ale_php_phpcbf_standard = "PSR12"

" autocmd FileType php set isKeyword+=$
autocmd FileType php setlocal omnifunc=phpactor#Complete
