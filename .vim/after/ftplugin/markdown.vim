setlocal textwidth=80
setlocal formatoptions+=t
setlocal nowrap
setlocal spell
setlocal makeprg=blackfriday-tool\ %\ %:r.html
nnoremap <buffer> <LocalLeader>sh "zyy"zpVr-
nnoremap <buffer> <LocalLeader>h "zyy"zpVr=
inoremap <buffer> * *<space><space><space>
nnoremap <buffer> <LocalLeader>ts V:!date +\%Y-\%m-\%d<cr>
