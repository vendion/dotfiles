autocmd FileType scss setl iskeyword+=@-@

setlocal tabstop=2 softtabstop=2 expandtab shiftwidth=2 smarttab
