setlocal textwidth=79
setlocal ts=2 sts=2 sw=2

let b:ale_linters = ['shellcheck', 'shell', 'cspell']
