setlocal shiftwidth=2
setlocal tabstop=2
setlocal softtabstop=2

if has("autocmd")
         let pandoc_pipeline  = "pandoc --from=html --to=markdown"
         let pandoc_pipeline .= " | pandoc --from=markdown --to=html"
         let &formatprg=pandoc_pipeline
endif
