call govim#config#Set("Gofumpt", 1)
call govim#config#Set("Staticcheck", 1)
call govim#config#Set("Analyses", {"fillstruct": 1})
call govim#config#Set("FormatOnSave", "goimports-gofmt")

nmap <silent> <buffer> <F5> :execute "GOVIMQuickfixDiagnostics" | cw | if len(getqflist()) > 0 && getwininfo(win_getid())[0].quickfix == 1 | :wincmd p | endif<CR>
imap <silent> <buffer> <F5> <C-O>:execute "GOVIMQuickfixDiagnostics" | cw | if len(getqflist()) > 0 && getwininfo(win_getid())[0].quickfix == 1 | :wincmd p | endif<CR>

nmap <silent> <buffer> <LocalLeader>gh : <C-u>call GOVIMHover()<CR>

let b:ale_linters = ['cspell', 'golangci_lint']
