""""""""""""""""""""""""""""""""""""""""
"                                      "
"            Remap Commands            "
"                                      "
""""""""""""""""""""""""""""""""""""""""

let mapleader = "\<Space>"
let maplocalleader = "\\"

nmap <silent> <Leader>ve :tabedit $MYVIMRC<CR>
nmap <silent> <Leader>vs :source $MYVIMRC<CR>
nmap <silent> <Leader>vc :tabedit ~/.vim/coc-settings.json<CR>

nnoremap <Leader>w :w<CR>

" Allow gf to open non-existent files
map gf :edit <cfile><CR>

nmap <Left> <<
nmap <Right> >>
vmap <Left> <gv
vmap <Right> >gv
nmap <Up> [e
nmap <Down> ]e
vmap <Up> [egv
vmap <Down> ]egv

nmap zz yygccp
vmap zz V<Esc>gvygvgc`>p

nnoremap <C-e> :e#<CR>

nmap <F4> :set spell!<CR>
nmap <LocalLeader>s z=

vnoremap <silent> y y`]
vnoremap <silent> p p`]
nnoremap <silent> p p`]

noremap gV `[v`]

nnoremap <Leader>] :bw <CR>

nnoremap <silent> <Leader>bp :bprevious<CR>
nnoremap <silent> <Leader>bn :bnext<CR>
nnoremap <silent> <Leader>bu <c-^>
nnoremap <silent> <Leader>bq :bufdo bdelete<CR>

" Jumplist mutations
noremap <expr> j (v:count > 5 ? "m'" . v:count : "") . 'j'
noremap <expr> k (v:count > 5 ? "m'" . v:count : '') . 'k'

" Keep it centered
nnoremap n nzzzv
nnoremap N Nzzzv
nnoremap J mzJ`z

" Open the current file in the default program
nmap <Leader>x :!xdg-open %<CR><CR>

" Maintain the cursor position when yanking a visual selection
" http://ddrscott.github.io/blog/2016/yank-without-jank/
vnoremap y myy`y
vnoremap Y myY`y

nnoremap <Leader>h :wincmd h<CR>
nnoremap <Leader>j :wincmd j<CR>
nnoremap <Leader>k :wincmd k<CR>
nnoremap <Leader>l :wincmd l<CR>

" Make "Y" work like other capital commands
nnoremap Y y$

" Undo break points
inoremap , ,<c-g>u
inoremap . .<c-g>u
inoremap ! !<c-g>u
inoremap ? ?<c-g>u

" Moving text
vnoremap J :m '>+1<CR>gv=gv
vnoremap K :m '<-2<CR>gv=gv
inoremap <C-j> <ESC>:m .+1<CR>==
inoremap <C-k> <ESC>:m .-2<CR>==
nnoremap <Leader>mj :m .+1<CR>==
nnoremap <Leader>mk :m .-2<CR>==

" Preserve redigsters when cutting
vnoremap <Leader>p "_dp
