"""""""""""""""""""""""""""""""""""""""
"                                     "
"           Basic Setup               "
"                                     "
"""""""""""""""""""""""""""""""""""""""

""""""""""""""""""""""""""""""
"                            "
"        Appearance          "
"                            "
""""""""""""""""""""""""""""""
" Color Scheme stuff
if has('termguicolors')
    let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
    let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
    set t_Co=256
    set termguicolors
endif

colorscheme nord
augroup nord-theme-overrides
  let g:nord_italic = 1
  let g:nord_italic_comments = 1
  let g:nord_underline = 1
  " let g:nord_uniform_diff_background = 1
  " autocmd!
  " autocmd ColorScheme nord highlight Comment ctermfg=242 guifg=#ECEFF4
  " autocmd ColorScheme nord highlight LineNr ctermfg=242 guifg=#ECEFF4
augroup END

set list listchars=tab:▸\ ,trail:·

set number
set relativenumber

set cursorline
set hlsearch

set nolazyredraw

set showmatch

set linebreak

set background=dark

set signcolumn=yes

set colorcolumn=120

set nowrap

""""""""""""""""""""""""""""""
"                            "
"        Folding             "
"                            "
""""""""""""""""""""""""""""""
set foldmethod=syntax
set foldnestmax=3
set nofoldenable

""""""""""""""""""""""""""""""
"                            "
"        Highlighting        "
"                            "
""""""""""""""""""""""""""""""
autocmd BufRead,BufNewFile *.elv set filetype=elvish
autocmd BufNewFile,BufRead *.migration set filetype=sql
highlight ColorColumn ctermbg=0 guibg=LightCyan

""""""""""""""""""""""""""""""
"                            "
"        Splits              "
"                            "
""""""""""""""""""""""""""""""
set splitbelow        " Horizontal split below current
set splitright        " Vertical split to right of current

""""""""""""""""""""""""""""""
"                            "
"        Scrolling           "
"                            "
""""""""""""""""""""""""""""""
if !&scrolloff
        set scrolloff=3   " Show next 3 lines while scrolling
endif

if !&sidescrolloff
        set sidescrolloff=5 " Show next 5 columns while side-scrolling
endif

""""""""""""""""""""""""""""""
"                            "
"        Indentations        "
"                            "
""""""""""""""""""""""""""""""
set autoindent
set smartindent
set smarttab
set expandtab
set copyindent

""""""""""""""""""""""""""""""
"                            "
"        Searching           "
"                            "
""""""""""""""""""""""""""""""
set nohlsearch
set ignorecase
set smartcase
set incsearch
set magic
set viminfo='100,f1
set gdefault

""""""""""""""""""""""""""""""
"                            "
"        Undo                "
"                            "
""""""""""""""""""""""""""""""
if version >= 703
        if isdirectory($HOME . '/.vim/undodir') == 0
                :silent !mkdir -p ~/.vim/undodir >/dev/null 2>&1
        endif
        set undodir=~/.vim/undodir
        set undofile
        set undolevels=1000  " Maximum number of changes that can be undone
        set undoreload=10000 " Maximum number line to save for undo on a buffer reload
endif

""""""""""""""""""""""""""""""
"                            "
"        Paste               "
"                            "
""""""""""""""""""""""""""""""
set pastetoggle=<F10>
let g:clipbrdDefaultReg = '+'

""""""""""""""""""""""""""""""
"                            "
"        Completion          "
"                            "
""""""""""""""""""""""""""""""
set wildmode=longest:full,full
set wildoptions=pum

""""""""""""""""""""""""""""""
"                            "
"        Misc                "
"                            "
""""""""""""""""""""""""""""""
set noerrorbells
set vb
set t_vb="."

set encoding=utf-8

set spelllang=en_us,programming
set spellfile=$HOME/.vim/spell/en.utf-8.add
set spellsuggest=5

filetype plugin on
filetype indent on

" Having longer updatetime (default is 4000 ms = 4 s) leads to noticeable
" delays and poor user experience.
set updatetime=500
set redrawtime=20000 " Allow more time for loading syntax on large files
set timeoutlen=1000 ttimeoutlen=0

set sessionoptions-=options

if !empty(&viminfo)
        set viminfo^=!
endif

set exrc
set secure

exec "syntax sync minlines=200"

set autoread
set autowrite

set ttyfast

set isfname-=:
set isfname-==
set isfname-=+

set noswapfile

set mouse=a
set ttymouse=xterm2

set backspace=2

if has("patch-8.1.1904")
      set completeopt+=popup
      set completepopup=align:menu,border:off,highlight:Pmenu
endif

set hidden

set nobackup
set nowritebackup

set cmdheight=2

set shortmess+=c

let g:netrw_browse_split=2
let g:netrw_banner=0
let g:newrw_winsize=25
