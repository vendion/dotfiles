
use strict;
use Irssi;
use vars qw($VERSION %IRSSI);
use HTML::Entities;
# The character encoding of the Irssi client:
use constant ENCODING => "UTF-8";

our $VERSION= "0.1.1";
our %IRSSI = (
  name => "Notify Mention",
  description => "Displays a desktop notification when meantioned on a channel.",
  authors => "Adam Jimerson",
  contact => "vendion\@gmail.com",
  license => "",
  changed => "Wed 17 Aug 10:45:00 EST 2016",
);

# This is heavily based on https://github.com/jhradilek/irssi-notifications/blob/master/notifications.pl and [place holder for irssi-libnotify]

sub display_notification {
  my ($server, $summary, $body) = @_;

  $summary = sanitize($summary);
  $body = sanitize($body);
  my $daemon = Irssi::settings_get_str('notify_mention_daemon');
  my $cmd = '';
  if ($daemon eq 'twmn') {
    $cmd = 'EXEC twmnc --icon chat_icon --title ' . $summary . ' --content '
    . $body;
  } elsif ($daemon eq 'osx') {
    $cmd = 'EXEC osascript -e \'display notification "' . $body . '" with title "'
    . $summary . '" sound name "Basso"\''
  }
  $server->command($cmd);
}

sub sanitize {
  my ($text) = $_;
  encode_entities($text, '\'<>&');
  my $apos = "&#39;";
  my $aposenc = "\&apos;";
  $text =~ s/$apos/$aposenc/g;
  $text =~ s/"/\\"/g;
  $text =~ s/\$/\\\$/g;
  $text =~s/`/\\"/g;
  return $text;
}

sub public_msg {
  my ($server, $msg, $nick, $address) = @_;

  # Check whether to notify the user about public messages:
  return unless (Irssi::settings_get_bool('notify_mention_public_messages'));

  # Check whether to notify the user about messages in the active window:
  unless (Irssi::settings_get_bool('notify_mention_active_window')) {
    # Get the name of the active window:
    my $window = Irssi::active_win()->{active}->{name} || '';

    # Ignore messages in the active window:
    return if ($window eq $nick);
  }

  # Get the server's tag:
  my $tag = $server->{tag};

  # Notify the user about the incoming private message:
  display_notification($server, "Message from $nick/$tag:", $msg);
}

sub priv_msg {
  my ($server, $msg, $nick, $address) = @_;

  # Check whether to notify the user about public messages:
  return unless (Irssi::settings_get_bool('notify_mention_public_messages'));

  # Check whether to notify the user about messages in the active window:
  unless (Irssi::settings_get_bool('notify_mention_active_window')) {
    # Get the name of the active window:
    my $window = Irssi::active_win()->{active}->{name} || '';

    # Ignore messages in the active window:
    return if ($window eq $nick);
  }

  # Get the server's tag:
  my $tag = $server->{tag};

  # Notify the user about the incoming private message:
  display_notification($server, "Message from $nick/$tag:", $msg);
}

sub dcc_request {
  my ($dcc, $sendaddr) = @_;
  my $server = $dcc->{server};

  # Check whether to notify the user about DCC requests:
  return unless (Irssi::settings_get_bool('notify_mention_dcc_messages'));

  # Check whether to notify the user about messages in the active window:
  unless (Irssi::settings_get_bool('notify_mention_active_window')) {
    # Get the name of the active window:
    my $window = Irssi::active_win()->{active}->{name} || '';

    # Ignore messages in the active window:
    return unless ($window);
  }

  # Get the request type:
  my $type = $dcc->{type};

  # Get the sender's nick:
  my $nick = $dcc->{nick};

  # Get the server's tag:
  my $tag  = $dcc->{server}->{tag};

  # Check the request type:
  if ($type eq 'GET') {
    # Get the file name and size:
    my $name = $dcc->{arg};
    my $size = $dcc->{size};

    # Notify the user about the incoming SEND request:
    display_notification($server, "$nick/$tag offers a file:", "$name ($size B)");
  }
  elsif ($type eq 'CHAT') {
    # Notify the user about the incoming CHAT request:
    display_notification($server, "$nick/$tag offers a DCC chat.", "");
  }
}

sub dcc_chat_msg {
  my ($dcc, $msg) = @_;
  my $server = $dcc->{server};

  # Check whether to notify the user about DCC requests:
  return unless (Irssi::settings_get_bool('notify_mention_dcc_messages'));

  # Get the sender's nick:
  my $nick = $dcc->{id};

  # Check whether to notify the user about messages in the active window:
  unless (Irssi::settings_get_bool('notify_mention_active_window')) {
    # Get the name of the active window:
    my $window = Irssi::active_win()->{active}->{name} || '';

    # Ignore messages in the active window:
    return if ($window eq "=$nick");
  }

  # Get the server's tag:
  my $tag  = $dcc->{server}->{tag};

  # Notify the user about the incoming CHAT message:
  display_notification($server, "DCC chat message from $nick/$tag:", $msg);
}

# Register configuration options:
Irssi::settings_add_bool('notify_mention', 'notify_mention_private_messages', 1);
Irssi::settings_add_bool('notify_mention', 'notify_mention_public_messages',  1);
Irssi::settings_add_bool('notify_mention', 'notify_mention_indirect_messages',0);
Irssi::settings_add_bool('notify_mention', 'notify_mention_active_window', 0);
Irssi::settings_add_bool('notify_mention', 'notify_mention_dcc_messages',  1);
Irssi::settings_add_str('notify_mention', 'notify_mention_daemon', '');

# Register signals:
Irssi::signal_add('message public',   'message_public');
Irssi::signal_add('message private',  'message_private');
Irssi::signal_add('dcc request',      'dcc_request');
Irssi::signal_add('dcc chat message', 'dcc_chat_message');
